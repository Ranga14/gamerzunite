<!DOCTYPE html>
<html lang="en">

<?php include_once('./includes/header.includes.php'); ?>

<head>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<meta name="keywords" content="Online Gaming Community, Online Gaming, Games, Gamers, Online Games, Gaming News, Lan Party, Lan Parties, 
    Lans, Lan, Gaming Events, Game Events, dallas lans, dallas lan parties, dfw lans, dfw lan parties" />
    
    <meta name="description" content="Gamerz Unite - How to host a Lan Party or Lan Event of your own!" />
    
    <meta name="verify-v1" content="oIgbVOUJX3dUtEFWlowqGg3+TT9cQF/AJ6fLLRYjZII=" /> 
    
    <!-- Title Tag -->
	<title>Gamerzunite.com Privacy Policy</title>
    
    <!-- Bootstrap -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/tb_overrides.css" rel="stylesheet">
    <link href="./css/tb_overrides_content.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <?php include_once('./google-analytics.php'); ?>

</head>

<body>

	<?php include_once('./navigation-main.php'); ?>
    
    <div class="container">
    	
        <div class="row inner">
        
        	<!-- Begin Right Column -->
        	<div class="col-md-9 inner">
                
                <div class="bg-primary hdr_push">
                    <h1>Gamerzunite.com Disclaimer</h1>
                    <h2></h2>
                </div>
                
                <!-- Begin Main Content -->
                <div class="col-lg-12">
                    
                    <p>Gamerzunite.com is not responsible for any incorrect or inaccurate content posted on the site or in connection with the Gamerzunite.com service, whether caused by users, 
                    members or by any of the equipment or programming associated with or utilized in the service, nor for the conduct of any user and/or member of the Gamerzunite.com 
                    service whether online or offline.</p>
                    
                    <p>Gamerzunite.com assumes no responsibility for any error, omission, interruption, deletion, defect, delay in operation or transmission, 
                    communications line failure, theft or destruction or unauthorized access to, or alteration of, user and/or member communications. </p>
                    
                    <p>Gamerzunite.com is not responsible 
                    for any problems or technical malfunction of any telephone network or lines, computer on-line-systems, servers or providers, computer equipment, software, failure of 
                    email or players on account of technical problems or traffic congestion on the Internet or at any website or combination thereof, including injury or damage to users 
                    and/or members or to any other person's computer related to or resulting from participating or downloading materials in connection with the Gamerzunite.com site 
                    and/or in connection with the Gamerzunite.com Service. </p>
                    
                    <p>Under no circumstances will Gamerzunite.com be responsible for any loss or damage resulting from anyone's use 
                    of the site or the service and/or any content posted on the Gamerzunite.com site or transmitted to Gamerzunite.com members. The site and the service are provided 
                    "AS-IS" and Gamerzunite.com expressly disclaims any warranty of fitness for a particular purpose or non-infringement. Gamerzunite.com cannot guarantee and does not 
                    promise any specific results from use of the site.</p>
                    
                </div>
                <!-- End Main Content -->
            
            </div>
            <!-- End Left Column -->

			<!-- Begin Right Column -->
            <div class="col-md-3">
                
                <?php include_once('./navigation-side.php'); ?>
            
            </div>
            <!-- End Right Column -->
        
        </div>
    
    </div>
    <!-- End Main Container -->

	<?php include_once('./footer.php'); ?>
    
    <?php include_once('./global-js.php'); ?>
    
</body>

</html>