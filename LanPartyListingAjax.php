<?php
include_once('./includes/header.includes.php');
include_once('LanPartyListingProcessor.php');

//Simple Data sanitation
foreach ($_GET as $key => $value) {
	$_GET[$key] = strip_tags($value);
}

$lan = new LanPartyListingProcessor();
$action = $_GET['action'];
$response = array();

switch($action) 
{
	case 'post_lan':
		$temp = $lan->post_lan($_GET);
		$response['status'] = 'success'; 
	break;
	case 'lans':
		$result_per_page = $_GET['result_per_page'];
		$page = $_GET['page'];
		$temp = $lan->get_lans();
		if($temp)
		{
			$response['status'] = 'success'; 
			$response['lans'] = $temp; 
		}
		else
		{
			$response['status'] = 'fail'; 
		}
	break;
	case 'lan_by_name':
		$name = $_GET['name'];
		$lan->get_lan_by_name($name);
		$temp = $lan->get_lans();
		if($temp)
		{
			$response['status'] = 'success'; 
			$response['lans'] = $temp; 
		}
		else
		{
			$response['status'] = 'fail'; 
		}
	break;
	case 'lan_by_id':
		$id = $_GET['lan_id'];		
		$temp = $lan->get_lan_by_id($id);
		if($temp)
		{
			$response['status'] = 'success'; 
			$response['lans'] = $temp; 
		}
		else
		{
			$response['status'] = 'fail'; 
		}
	break;
	default:
		$response['status'] = 'fail'; 
	break;
}

echo(json_encode($response));