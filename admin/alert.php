<!DOCTYPE html>
<html lang="en">

<?php
	$host = "10.62.43.130";
	$port = "3306";
	$user = "adn_user";
	$password = "83nzi1477h!T";
	$dbname = "gu_alert";
	
	$mysqli = mysqli_init();
	//$mysqli->ssl_set('ssl/41908-client-key.pem', 'ssl/41908-client-cert.pem', 'ssl/ca-cert.pem', 'ssl', NULL);
	$mysqli->real_connect($host, $user, $password, $dbname, $port);
	if ($mysqli->connect_errno) {
		printf("Connection failed: %s\n", $mysqli->connect_error);
		exit();
	}
	//echo "Connected";
	//var_dump($mysqli);
						
	// Pull News & A Piece of Our Mind
	$Content			= "SELECT aID,a_message FROM alerts WHERE aID='1'";
	$ContentQuery		= mysqli_query($mysqli, $Content);
?>
<head>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">  
    
    <!-- Title Tag -->
	<title>GamerzUnite.com Admin Panel</title>
    
    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
		.success { display: block; margin: 12px 0; background-color: #bdf3be; padding: 8px; }
    	textarea { height: 100px; width: 400px; }
		.Submit { background-color: #ccc; border-radius: 2px; clear: both; display: block; border: 0 none; padding: 8px; font-weight: bold; margin-top: 4px; }
    </style>
    <script src="https://code.jquery.com/jquery.js"></script>

</head>

<body>

	<nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="../index.php">GamerzUnite.com</a>
            </div>
            
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
                <ul class="nav navbar-nav">
                    <li><a href="./index.php">Admin</a></li>
                    <li class="active"><a href="./alert.php">Update Alert</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
    
    <!-- Begin Main Container -->
	<div class="container">
    	                
        <div class="col-lg-12">
            
            <?php while($ContentData = mysqli_fetch_assoc($ContentQuery)): ?>
                
                <?php $url = $ThreadData['title']; ?>
                
                <div class="panel panel-default">
                    <div class="panel-heading">Edit your Alert Below...</div>
                    <div class="panel-body"> 
                    	<div class="success" style="display: none;">Alert Updated!</div>
                        <span class="tagline">
                            <form action="./objects/alert_update.php" method="post" id="tagline_edit<?php echo $ContentData['aID']; ?>">
                                <textarea name="msg" maxlength="200"><?php echo strip_tags($ContentData['a_message']); ?></textarea>
                                <input type="submit" name="submit" class="Submit" value="Update" />
                            </form>
                        </span>
                    </div>
                </div>
                
                <script type="text/javascript">
					$(document).ready(function(){
						$("#tagline_edit<?php echo $ContentData['aID']; ?>").on("submit", function(e) {
							var $btn = $(this).find(".Submit");
		
							$btn.attr("disabled", true);
							$btn.val("Updating...");
							
							e.preventDefault();
							$.ajax({
								type: "POST",
								data: $(this).serialize(),
								url: $(this).attr("action"),
								//url: $(".delete_yes").action,
								success: function(msg) {
									// Insert post
									$("#tagline_edit<?php echo $ContentData['aID']; ?> textarea").val(msg);
									
									// Re-enable submit button
									$btn.removeAttr("disabled");
		
									// Update button text
									$btn.val("UPDATE");
									
									// Update Alert Box
									$('.success').fadeIn(500);
									$('.success').fadeOut(5000);
								}
							});
						});
					});
				</script>
                                        
            <?php endwhile; ?>
            
        </div>
        <!-- News End -->
    
    </div>
    <!-- End Main Container -->
    
    <footer>
        
        <div class="col-lg-8">
        
        	&copy; GamerzUnite.com    
        
        </div>
    
    </footer>
    
    
	
    
</body>

</html>