<?php
class Paging {

	var $rpp; // records per page
	var $ppp; // pages per page
	var $total_rec; // total records
	var $query_string; // query string to be appended to each url
	var $page; // current page no
	var $bgcolor;

	function page_it() { // Paging Function
		if(!$this->ppp) $this->ppp =10; // if pages per page not specified default to 10
		if(!$this->rpp) $this->rpp =10; // if records per page not specified default to 10
		
		$no_of_pages = (int)($this->total_rec / $this->rpp); // calculate total no of pages 
		
		if($this->total_rec % $this->rpp) { $no_of_pages++; } // increament no of pages by one if not exactly divisible by rpp
		if($this->page < $no_of_pages) 
		{ 	
			// prepare next page link
			$nextpage=$this->page+1;
			$next_code = "<a href = '?page=".$nextpage.$this->query_string."' >&gt;</a>";
		}
		if($this->page > 1)
		{ 	
			// prepare previous page link
			$prevpage=$this->page-1;
			$prev_code = "<a href = '?page=".$prevpage.$this->query_string."' target='_parent'>&lt;</a>";
		}
		
		$startingpage = (int)($this->page - ($this->ppp/2)); if($startingpage <= 0) $startingpage= 1; // Calculate Starting page to be shown in the loop
		$endingpage = (int) ($this->page + ($this->ppp/2)); if($endingpage >= $no_of_pages) $endingpage= $no_of_pages; // Calculate Ending page to be shown in the loop
		
		for($i=$startingpage;$i < ($endingpage+1);$i++) 
		{ 
			// prepare the middle string for the paging
			//if($i != $startingpage) { $paging_code .= " | "; }
			if($i != $this->page) 
			{
				$paging_code .= "<a href = '?page=".$i.$this->query_string."'>$i</a>"; // append the query string to the url
			} else {
				$paging_code .= "<a href='#' class='Current'>$i</a>"; // for current page dont give hyperlink
			}
		}
	
		if ($no_of_pages == $this->page) 
		{
			$last_code = "";
		} else {
			$last_code = "<a href = '?page=".$no_of_pages.$this->query_string."'>Last <strong>&raquo;</strong></a>";
		}
	
		if ($this->page <= $this->ppp) 
		{
			$first_code = "";
		} else {
			$first_code = "<a href = '?page=1".$this->query_string."'><strong>&laquo;</strong> First</a>";
		}
	
		if($no_of_pages > 1) 
		{
			$tbl_start = "<div class='Pagination'><div class='Paging'>Page ".$this->page." of ".$no_of_pages."</div>";
			$tbl_end = "</div>";
			$paging_code = $tbl_start.$first_code.$prev_code.$paging_code.$next_code.$last_code.$tbl_end; // prepare the final paging code
		} else {
			$paging_code = "";
		}
		
		return($paging_code);
	}
}
?>