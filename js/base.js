$('.newsticker').newsTicker({
	row_height: 20,
	max_rows: 1,
	speed: 600,
	direction: 'up',
	duration: 4000,
	autostart: 1,
	pauseOnHover: 0
});

$('.form-control').focus(function() { 
	//$(this).empty();
	$(this).val(''); 
});

$('.search-gzu').blur(function() { 
	//$(this).empty();
	$(this).val('Search GZU'); 
	$(this).css('background', '#fff none');
});

// Define Container for Video
if ( $('.col-lg-12 div').hasClass('weekendRefresh') ) {
	var $videoContainer = $(".weekendRefresh");
} else {
	var $videoContainer = $(".col-lg-12");
}