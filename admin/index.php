<!DOCTYPE html>
<html lang="en">

<?php
	include_once('../includes/dbconnect.inc.php');
						
	// Pull News & A Piece of Our Mind
	$Content			= "SELECT tid,title,title_seo,description,start_date FROM ibf_topics WHERE forum_id='76' AND approved='1' OR forum_id='18' AND approved='1' ORDER BY start_date DESC LIMIT 5";
	$ContentQuery		= mysqli_query($mysqli, $Content);
?>
<head>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">  
    
    <!-- Title Tag -->
	<title>GamerzUnite.com Admin Panel</title>
    
    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script src="https://code.jquery.com/jquery.js"></script>

</head>

<body>

	<nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="../index.php">GamerzUnite.com</a>
            </div>
            
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="./index.php">Admin</a></li>
                    <li><a href="./alert.php">Update Alert</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
    
    <!-- Begin Main Container -->
	<div class="container">
    	                
        <div class="col-lg-12">
            
            <?php while($ContentData = mysqli_fetch_assoc($ContentQuery)): ?>
                
                <?php $url = $ThreadData['title']; ?>
                
                <div class="panel panel-default">
                    <div class="panel-heading"><a href="../<?php echo $ContentData['title_seo']; ?>" style="font-weight: bold;"><?php echo $ContentData['title']; ?></a></div>
                    <div class="panel-body">
                        <span class="glyphicon glyphicon-edit"></span> <a class="toggle_title<?php echo $ContentData['tid']; ?>">Edit Tagline Description</a> &mdash; 
                        <span class="tagline"><?php echo $ContentData['description']; ?></span>
                        <div class="edit_title_pop<?php echo $ContentData['tid']; ?>">
                            <form action="./objects/tagline_update.php" method="post" id="tagline_edit<?php echo $ContentData['tid']; ?>">
                                <input type="text" name="tagline" value="<?php echo $ContentData['description']; ?>" />
                                <input type="hidden" name="tID" value="<?php echo $ContentData['tid']; ?>" />
                                <input type="submit" name="submit" class="Submit" value="Update" />
                            </form>
                        </div>
                    </div>
                </div>
                
                <style type="text/css">
					.toggle_title<?php echo $ContentData['tid']; ?>					{ cursor: pointer; }
					.toggle_caption<?php echo $ContentData['tid']; ?>					{ cursor: pointer; }
					.edit_title_pop<?php echo $ContentData['tid']; ?>					{ position: absolute; display: none; clear: both; width: auto; margin: -18px 0 12px 0; padding: 12px; background-color: #fff; border: 1px solid #ccc; }
					.edit_title_pop<?php echo $ContentData['tid']; ?> input[type="text"] { width: 300px; }
					.edit_pop_show<?php echo $ContentData['tid']; ?>					{ display: block; }
				</style>
                <script type="text/javascript">
					$(document).ready(function(){
						$(".toggle_title<?php echo $ContentData['tid']; ?>").click(function(){
							$(".edit_title_pop<?php echo $ContentData['tid']; ?>").toggleClass("edit_pop_show<?php echo $ContentData['tid']; ?>").fadeIn(500);
							$("#tagline_edit<?php echo $ContentData['tid']; ?>").on("submit", function(e) {
								var $btn = $(this).find(".Submit");
			
								$btn.attr("disabled", true);
								$btn.val("Updating...");
								
								e.preventDefault();
								$.ajax({
									type: "POST",
									data: $(this).serialize(),
									url: $(this).attr("action"),
									//url: $(".delete_yes").action,
									success: function(msg) {
										// Insert post
										$(".tagline<?php echo $ContentData['tid']; ?>").replaceWith(msg);
										
										// Re-enable submit button
										$btn.removeAttr("disabled");
			
										// Update button text
										$btn.val("UPDATE");
										
										// Fade Box Out
										$('.edit_title_pop<?php echo $ContentData['tid']; ?>').fadeOut(500);
									}
								});
							});
						});
						// Close the feed whenever you click out of the container.
						$(document).mouseup(function (e)
						{
							var containerDrop = $(".edit_title_pop<?php echo $ContentData['tid']; ?>");
						
							if (!containerDrop.is(e.target) // if the target of the click isn't the container...
								&& containerDrop.has(e.target).length === 0) // ... nor a descendant of the container
							{
								containerDrop.hide();
								//$("#Feed").empty();
							}
						});
					});
				</script>
                                        
            <?php endwhile; ?>
            
        </div>
        <!-- News End -->
    
    </div>
    <!-- End Main Container -->
    
    <footer>
        
        <div class="col-lg-8">
        
        	&copy; GamerzUnite.com    
        
        </div>
    
    </footer>
    
    
	
    
</body>

</html>