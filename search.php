<!DOCTYPE html>
<html lang="en">

<?php include_once('./includes/header.includes.php'); ?>

<head>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<meta name="keywords" content="games search results, games news, gaming news, game previews, game reviews, game opinions" />
	
	<meta name="description" content="Gamerz Unite - Games News, Previews, Reviews and Gaming Opinions." />
    
    <meta name="verify-v1" content="oIgbVOUJX3dUtEFWlowqGg3+TT9cQF/AJ6fLLRYjZII=" /> 
    
    <!-- Title Tag -->
	<title>Games Search Results | Games News | Lan Party</title>
    
    <!-- Bootstrap -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/tb_overrides.css" rel="stylesheet">
    <link href="./css/tb_overrides_content.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <?php include_once('./google-analytics.php'); ?>

</head>

<body>

	<?php include_once('./navigation-main.php'); ?>
    
    <div class="container">
    	
        <div class="row inner">
        
        	<!-- Begin Right Column -->
        	<div class="col-md-9 inner">
                
                <div class="bg-primary hdr_push">
                    <h1>GamerzUnite Search Results</h1>
                    <h2></h2>
                </div>
                
                <!-- Begin Main Content -->
                <div class="col-lg-12">
                    
                    <div id="cse-search-results"></div>
                    <script type="text/javascript">
                        var googleSearchIframeName = "cse-search-results";
                        var googleSearchFormName = "cse-search-box";
                        var googleSearchFrameWidth = 560;
						var googleSearchFrameHeight = 1000;
                        var googleSearchDomain = "www.google.com";
                        var googleSearchPath = "/cse";
                    </script>
                    <script type="text/javascript" src="http://www.google.com/afsonline/show_afs_search.js"></script>
                        
                    <div class="row comments">
                                                    
                        <h3>More GamerzUnite News</h3>
                        
                        <div class="contain">
                        
                            <?php 
                            // Pull News Thread Data
                            $News			= "SELECT tid,title,title_seo,description,posts,start_date,topic_firstpost FROM ibf_topics WHERE forum_id='18' AND approved='1' ORDER BY start_date DESC LIMIT 6";
                            $NewsQuery		= mysqli_query($mysqli, $News);
                            while($data 	= mysqli_fetch_assoc($NewsQuery)): 
                                
                                $url = $data['title'];
                                
                                // Pull Thumb Image for Article
                                $NewsImg			= "SELECT attach_thumb_location FROM ibf_attachments WHERE attach_rel_id='$data[topic_firstpost]'";
                                $NewsImgQuery		= mysqli_query($mysqli, $NewsImg);
                                $NewsImg			= mysqli_fetch_array($NewsImgQuery);
                                ?>
                                
                                <div class="col-sm-6 col-md-4">
                                    <div class="thumbnail">
                                        <a href="./<?php echo friendlyURL($url); ?>&amp;id=<?php echo $data['tid']; ?>"><img data-src="holder.js/300x200" src="../forums/uploads/<?php echo $NewsImg['attach_thumb_location']; ?>" alt="<?php echo $data['title']; ?>"></a>
                                        <div class="caption">
                                            <h3><a href="./<?php echo friendlyURL($url); ?>&amp;id=<?php echo $data['tid']; ?>"><?php echo $data['title']; ?></a></h3>
                                            <p><?php echo $data['description']; ?></p>
                                        </div>
                                        
                                    </div>
                                    <a href="./<?php echo friendlyURL($url); ?>&amp;id=<?php echo $data['tid']; ?>" class="btn btn-primary" role="button">Read More</a>
                                </div>
                            
                            <?php endwhile; ?>
                            
                        </div>
                        
                    </div>                    
                    
                </div>
                <!-- End Main Content -->
            
            </div>
            <!-- End Left Column -->

			<!-- Begin Right Column -->
            <div class="col-md-3">
                
                <?php include_once('./navigation-side.php'); ?>
            
            </div>
            <!-- End Right Column -->
        
        </div>
    
    </div>
    <!-- End Main Container -->

	<?php include_once('./footer.php'); ?>
    
    <?php include_once('./global-js.php'); ?>
    
</body>

</html>