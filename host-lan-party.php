<!DOCTYPE html>
<html lang="en">

<?php include_once('./includes/header.includes.php'); ?>

<head>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<meta name="keywords" content="host lan party, hosting your own lan party, host a lan party, how to host a lan, how to host a lan party, what equipment do i need for a lan, lan party host,
    lan party hosting, what to get to host a lan party, lan party 101, lan party hosting 101, lan party hosting basics" />
	
	<meta name="description" content="Every wondered how to host a lan party? Well, here's a few tips that might save you some time..." />
    
    <meta name="verify-v1" content="oIgbVOUJX3dUtEFWlowqGg3+TT9cQF/AJ6fLLRYjZII=" /> 
    
    <!-- Title Tag -->
	<title>How to Host a Lan Party | Lan Party Games | Setup Lan Party | How to Lan Game</title>
    
    <!-- Bootstrap -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/tb_overrides.css" rel="stylesheet">
    <link href="./css/tb_overrides_content.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <?php include_once('./google-analytics.php'); ?>

</head>

<body>

	<?php include_once('./navigation-main.php'); ?>
    
    <div class="container">
    	
        <div class="row inner">
        
        	<!-- Begin Right Column -->
        	<div class="col-md-9 inner">
                
                <div class="bg-primary hdr_push">
                    <h1>Setup Lan Party and Lan Games</h1>
                    <h2>Tips on Hosting a Lan Party and Finding Lan Games</h2>
                </div>
                
                <!-- Begin Main Content -->
                <div class="col-lg-12">
                    
                    <p>Below we have put together some helpful information on how to host a lan party. Lan Parties range in size greatly from just a few friends to thousands of gamers
                    coming together at convention centers. While most of us could probably never afford the later, we'll focus on the small to mid size parties. For a list of 
                    <strong>Lan Games</strong> played at lan parties, check out our discussion on them here: <a href="./forums/index.php?showtopic=277">Lan Party Games</a>.</p>
                    
                    <p><strong>Small (4-16 People)</strong></p>
                    
                    <p>Usually for small lan parties you can probably squeeze up to 16, maybe 20 people into your own home and see how it works. One crucial item to remember is that most
                    gamers that play often usually have decent rigs that suck a lot of power so you'll need to separate your friends into separate rooms to offset the load or you'll
                    overload a circuit. Yipes!</p>
                    
                    <p><strong>Small-Medium (16-50 People)</strong></p>
                    
                    <p>A lan of this size gets a little more tricky since you have friends that have brought friends plus several people you don't know as well. You should follow the same
                    guidelines as above but you'll obviously need more room. So unless you have a mansion your best bet is to rent out a small warehouse, hotel meeting area or some kind
                    of cheap office space. Since you won't have to worry about power as much since the facality will most likely have it covered (Ask to make sure), your main concern is
                    those that have come to this lan event. Keep a close eye on everyone attending and have friends help with this if possible. It's always better to be safe than sorry if
                    you return to frag some people and you see your rig missing! :O</p>
                    
                    <p><strong>Medium (50-200 People)</strong></p>
                    
                    <p>Once you have this many people coming to a lan event you might be hosting, it would be a good thing to start charging since you're probably paying an arm and a leg for 
                    the space and power consumption, so unless you have sponsers, it's usually a good idea to charge at least $10 a head to recoupe costs.</p>
                    
                    <h3>Helpful Tips:</h3>
                    
                    <ul style="list-style: square; margin-bottom: 0;">
                        <li>Make sure to plan your event and select game types, RTS &amp; FPS are most popular.</li>
                        <li>Be sure to encourage gamers to take breaks.</li>
                        <li>Order pizza and have soda/energy drinks available for free or sale, your choice.</li>
                        <li>Be sure to have a server available on the network for gamers to download the latest patches of games you'll play.</li>
                        <li>Have spare power &amp; network cords on hand in case someone forgets theirs.</li>
                        <li>Be nice to your guests. (The ones you don't know, the nicer you are, the more likely they are to return.)</li>
                    </ul>
                    
                    <p><strong>Resourceful Links for finding what you need:</strong></p>
                    
                    <p><a href="http://www.craigslist.org/">Craigslist.org &ndash; We suggest looking here for renting space.</a><br />
                    <a href="http://www.newegg.com/Store/Network.aspx?name=Networking">NewEgg &ndash; Networking Equipment that you "will" need!</a></p>
                    
                </div>
                <!-- End Main Content -->
            
            </div>
            <!-- End Left Column -->

			<!-- Begin Right Column -->
            <div class="col-md-3">
                
                <?php include_once('./navigation-side.php'); ?>
            
            </div>
            <!-- End Right Column -->
        
        </div>
    
    </div>
    <!-- End Main Container -->

	<?php include_once('./footer.php'); ?>
    
    <?php include_once('./global-js.php'); ?>
        
</body>

</html>