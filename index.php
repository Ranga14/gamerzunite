<!DOCTYPE html>
<html lang="en">

<?php
	include_once('./includes/header.includes.php');
						
	// Pull News & A Piece of Our Mind
	$Content			= "SELECT tid,title,title_seo,description,start_date,topic_firstpost,approved FROM ibf_topics WHERE forum_id='76' AND approved='1' OR forum_id='18' AND approved='1' ORDER BY start_date DESC LIMIT 5";
	$ContentQuery		= mysqli_query($mysqli, $Content);
	
	// Pull Video F5 Weekend Refresh
	//$Video				= "SELECT tid,title,title_seo,description,start_date,topic_firstpost,approved FROM ibf_topics WHERE forum_id='86' AND approved='1' ORDER BY start_date DESC LIMIT 5";
	//$VideoQuery			= mysqli_query($mysqli, $Video);
	//$videoCount			= mysql_num_rows($VideoQuery);
	
	// Pull News Thread Data
	$Thread				= "SELECT tid,title,title_seo,description,posts,start_date,topic_firstpost FROM ibf_topics WHERE forum_id='18' AND approved='1' ORDER BY start_date DESC LIMIT 9";
	$ThreadQuery		= mysqli_query($mysqli, $Thread);
			
	// Pull Lan (U.S.) Data
	$Lan				= "SELECT tid,title,title_seo,posts,forum_id FROM ibf_topics 
						   WHERE forum_id='5' OR forum_id='6' OR forum_id='9' OR forum_id='11' OR forum_id='12' OR forum_id='13' OR forum_id='46'
						   OR forum_id='40' OR forum_id='41' OR forum_id='42' OR forum_id='43' OR forum_id='44' OR forum_id='45' OR forum_id='68'
						   OR forum_id='70' OR forum_id='69' OR forum_id='71' OR forum_id='72' OR forum_id='78' OR forum_id='80' OR forum_id='87'
						   OR forum_id='88' OR forum_id='89'
						   ORDER BY start_date DESC LIMIT 10";
	$LanQuery			= mysqli_query($mysqli, $Lan);
	
	// Pull Lan (UK) Data
	$LanUK				= "SELECT tid,title,title_seo,posts,forum_id FROM ibf_topics WHERE forum_id='8' OR forum_id='37' ORDER BY start_date DESC LIMIT 8";
	$LanUKQuery			= mysqli_query($mysqli, $LanUK);
	
	// Pull Lan (UK) Data
	$LanCanada			= "SELECT tid,title,title_seo,posts,forum_id FROM ibf_topics WHERE forum_id='7' ORDER BY start_date DESC LIMIT 8";
	$LanCanadaQuery		= mysqli_query($mysqli, $LanCanada);
	
	// Pull Latest Forum Post (General)
	$DisGen				= "SELECT tid,title,title_seo,posts,forum_id FROM ibf_topics WHERE forum_id='14' ORDER BY start_date DESC LIMIT 8";
	$DisGenQuery		= mysqli_query($mysqli, $DisGen);
	
	// Pull Latest Forum Post (Console)
	$DisConsole			= "SELECT tid,title,title_seo,posts,forum_id FROM ibf_topics WHERE forum_id='15' ORDER BY start_date DESC LIMIT 8";
	$DisConsoleQuery	= mysqli_query($mysqli, $DisConsole);
	
	// Pull Latest Forum Post (PC)
	$DisPC				= "SELECT tid,title,title_seo,posts,forum_id FROM ibf_topics WHERE forum_id='16' ORDER BY start_date DESC LIMIT 8";
	$DisPCQuery			= mysqli_query($mysqli, $DisPC);
	
	// Pull Latest Forum Post (Online)
	$DisOnline			= "SELECT tid,title,title_seo,posts,forum_id FROM ibf_topics WHERE forum_id='17' ORDER BY start_date DESC LIMIT 8";
	$DisOnlineQuery		= mysqli_query($mysqli, $DisOnline);
	
	// Pull Latest Forum Post (Handheld)
	$DisHand			= "SELECT tid,title,title_seo,posts,forum_id FROM ibf_topics WHERE forum_id='25' ORDER BY start_date DESC LIMIT 8";
	$DisHandQuery		= mysqli_query($mysqli, $DisHand);
?>
<head>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<meta name="keywords" content="Online Gaming Community, Online Gaming, Games, Gamers, Online Games, Gaming News, Lan Party, Lan Parties, 
    Lans, Lan, Gaming Events, Game Events, dallas lans, dallas lan parties, dfw lans, dfw lan parties, lan party games, play free games" />
	
    <meta name="description" content="Gamerz Unite - An Online Gaming Community for Gamers to find Lan Parties in their area and read Video Game News." />
    
    <meta name="verify-v1" content="oIgbVOUJX3dUtEFWlowqGg3+TT9cQF/AJ6fLLRYjZII=" />    
    
    <!-- Title Tag -->
	<title>Online Gaming Community | Lan Parties | Lan Party Directory | Video Game News</title>
    
    <!-- Bootstrap -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/tb_overrides.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <?php include_once('./google-analytics.php'); ?>

</head>

<body>

	<?php include_once('./navigation-main.php'); ?>
    
    <!-- Begin Main Container -->
	<div class="container">
    	
        <div class="row">
        
        	<div class="col-md-9">
            	
                <!-- Rotate Image News -->
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                    </ol>
                    
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                    
                        <?php while($ContentData = mysqli_fetch_assoc($ContentQuery)):
							
							$url = $ContentData['title'];
							
							// Pull Large Image for Article
							$ContentImg			= "SELECT attach_location FROM ibf_attachments WHERE attach_rel_id='$ContentData[topic_firstpost]'";
							$ContentImgQuery	= mysqli_query($mysqli, $ContentImg);
							$ContentImg			= mysqli_fetch_array($ContentImgQuery);
	
							if ($ContentData['tid'] != 301 && $ContentData['approved'] == 1): ?>
                            
                                <div class="item">
                                    <a href="./<?php echo $ContentData['title_seo']; ?>"><img src="./forums/uploads/<?php echo $ContentImg['attach_location']; ?>" alt="<?php echo $ContentData['title']; ?>"></a>
                                    <div class="carousel-caption">
                                        <h3><a href="./<?php echo $ContentData['title_seo']; ?>"><?php echo $ContentData['title']; ?></a></h3>
                                        <p><?php echo $ContentData['description']; ?></p>
                                    </div>
                                </div>
                                
							<?php endif; ?>
                            
                        <?php endwhile; ?>
                        
                    </div>
    				
                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                    
                </div>
                <!-- End Rotate Image News -->
                
                <!-- News Start-->
                <div class="bg-primary">
                    <img src="./img/news_title.png" class="img-responsive" alt="From the depths of Nerddom Gamerzunite News">
                </div>
        
        		<!--<div class="contentScroller"> -->
                    
                    <div class="col-lg-12">
                        
                        <?php while($ThreadData = mysqli_fetch_assoc($ThreadQuery)): 
                            
							$url = $ThreadData['title'];
							
                            // Pull Thumb Image for Article
                            $NewsImg			= "SELECT attach_thumb_location FROM ibf_attachments WHERE attach_rel_id='$ThreadData[topic_firstpost]'";
                            $NewsImgQuery		= mysqli_query($mysqli, $NewsImg);
                            $NewsImg			= mysqli_fetch_array($NewsImgQuery);
                            ?>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail">
                                    <a href="./<?php echo $ThreadData['title_seo']; ?>"><img ThreadData-src="holder.js/300x200" src="./forums/uploads/<?php echo $NewsImg['attach_thumb_location']; ?>" alt="<?php echo $ThreadData['title']; ?>"></a>
                                    <div class="caption">
                                        <h3><a href="./<?php echo $ThreadData['title_seo']; ?>"><?php echo $ThreadData['title']; ?></a></h3>
                                        <p><?php echo $ThreadData['description']; ?></p>
                                    </div>
                                </div>
                                <div class="btn_grey_box"><?php $postDate = strftime("%B %#d<span></span> @ %I:%M %p", $ThreadData['start_date']); echo "$postDate"; ?></div>
                                <!--<a href="./<?php //echo $ThreadData['title_seo']; ?>" class="btn btn-primary" role="button">Read More</a>-->
                                <?php 
         //                            $postDay = strftime("%#d", $ThreadData['start_date']);
								
									// if ($postDay = 3) {
									// 	echo 'rd';
									// }
								?>
								<script>
                                    // $(document).ready(function() {
                                    //     console.log('testing');
                                    //     console.log('test: ' + '<?php echo $postDay ?>');
                                    //     if ( <?php echo $postDay ?> == 3 ) {
                                    //         $('<?php echo $postDay ?>rd').appendTo('span')
                                    //     }
                                    // });
                                </script>
                            </div>
                        
                        <?php endwhile; ?>
                        
                        <a href="./video-game-news.php" class="more_news">More News</a>
                        
                    </div>
                    <!-- News End -->
                    
                <!--</div> -->
                
                <div class="bg-info heading_override">
                    <h2>The Nerd Haven</h2>
                    <h1>GamerzUnite Lan Party and Game Event Listings</h1>
                </div>
                
                <!-- Begin Lan Party Listings -->
                <div class="col-lg-12">

                    <ul class="nav nav-tabs nav-justified" id="myTab">
                        <li class="active"><a href="#usa" data-toggle="tab">US Lan Parties</a></li>
                        <li><a href="#uk" data-toggle="tab">UK Lan Parties</a></li>
                        <li><a href="#canada" data-toggle="tab">Canada Lan Parties</a></li>
                    </ul>
                    
                    <div class="tab-content">
                        <div class="tab-pane active" id="usa">
                        
                        	<!-- Table US -->
                            <table class="table">
                            	<thead>
                                	<tr>                                        
                                        <th>Lan Party</th>
                                        <th>Discuss</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php while($LanData = mysqli_fetch_array($LanQuery)): ?>
                                        <tr>
                                            <td><a href="./forums/index.php?/topic/<?php echo $LanData['tid']; ?>-<?php echo $LanData['title_seo']; ?>"><?php echo $LanData['title']; ?></a></td>
                                            <td><?php echo number_format($LanData['posts']);  ?></td>
                                        </tr>
                                    <?php endwhile; ?>
                                    <tr>
                                    	<td colspan="2"><a href="./forums/index.php?showforum=5">More</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        
                        </div>
                        <div class="tab-pane" id="uk">
                        
                        	<!-- Table UK -->
                            <table class="table">
                            	<thead>
                                	<tr>                                        
                                        <th>Lan Party</th>
                                        <th>Discuss</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php while($LanData = mysqli_fetch_array($LanUKQuery)): ?>
                                        <tr>
                                            <td><a href="./forums/index.php?/topic/<?php echo $LanData['tid']; ?>-<?php echo $LanData['title_seo']; ?>"><?php echo $LanData['title']; ?></a></td>
                                            <td><?php echo number_format($LanData['posts']);  ?></td>
                                        </tr>
                                    <?php endwhile; ?>
                                    <tr>
                                    	<td colspan="2"><a href="./forums/index.php?showforum=5">More</a></td>
                                    </tr>
                                </tbody>
                            </table>
                            
                        </div>
                        <div class="tab-pane" id="canada">
                        
                        	<!-- Table Canada -->
                            <table class="table">
                            	<thead>
                                	<tr>                                        
                                        <th>Lan Party</th>
                                        <th>Discuss</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php while($LanData = mysqli_fetch_array($LanCanadaQuery)): ?>
                                        <tr>
                                            <td><a href="./forums/index.php?/topic/<?php echo $LanData['tid']; ?>-<?php echo $LanData['title_seo']; ?>"><?php echo $LanData['title']; ?></a></td>
                                            <td><?php echo number_format($LanData['posts']);  ?></td>
                                        </tr>
                                    <?php endwhile; ?>
                                    <tr>
                                    	<td colspan="2"><a href="./forums/index.php?showforum=5">More</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        
                        </div>
                    </div>
                    
                </div>
                <!-- End Lan Party Listings -->
                
                <!--<div class="panel panel-default">
                    <div class="panel-body">
                        LAN parties and online gaming continues to grow in popularity all over the world. For our Canadian readers, you can find <a href="http://www.casino.org/canada/">more information 
                        here</a> on games to play in Canada. <a href="http://www.pokersites.com/united-states/">In the United States</a>, many sites have a ton of great poker and other card games 
                        available to players along with comprehensive game strategy guides and teaching tools for beginners.
                    </div>
                </div>-->
                
                <div class="bg-info">
                    <h2>The Gamer Pitstop</h2>
                    <h3>GamerzUnite Discussion Forum</h3>
                </div>
                
                <!-- Begin Forum Listings -->
                <div class="col-lg-12">

                    <ul class="nav nav-tabs nav-justified" id="myTab">
                        <li class="active"><a href="#general" data-toggle="tab">General</a></li>
                        <li><a href="#console" data-toggle="tab">Console</a></li>
                        <li><a href="#pc" data-toggle="tab">PC</a></li>
                        <li><a href="#online" data-toggle="tab">Online</a></li>
                        <li><a href="#handheld" data-toggle="tab">Handheld</a></li>
                    </ul>
                    
                    <div class="tab-content">
                        <div class="tab-pane active" id="general">
                        
                        	<!-- Table General -->
                            <table class="table">
                            	<thead>
                                	<tr>                                        
                                        <th>Thread</th>
                                        <th>Discuss</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php while($forumData = mysqli_fetch_array($DisGenQuery)): ?>
                                        <tr>
                                            <td><a href="./forums/index.php?/topic/<?php echo $forumData['tid']; ?>-<?php echo $forumData['title_seo']; ?>"><?php echo $forumData['title']; ?></a></td>
                                            <td><?php echo number_format($forumData['posts']);  ?></td>
                                        </tr>
                                    <?php endwhile; ?>
                                    <tr>
                                    	<td colspan="2"><a href="./forums/index.php?showforum=5">More</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        
                        </div>
                        <div class="tab-pane" id="console">
                        
                        	<!-- Table Console -->
                            <table class="table">
                            	<thead>
                                	<tr>                                        
                                        <th>Thread</th>
                                        <th>Discuss</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php while($forumData = mysqli_fetch_array($DisConsoleQuery)): ?>
                                        <tr>
                                            <td><a href="./forums/index.php?/topic/<?php echo $forumData['tid']; ?>-<?php echo $forumData['title_seo']; ?>"><?php echo $forumData['title']; ?></a></td>
                                            <td><?php echo number_format($forumData['posts']);  ?></td>
                                        </tr>
                                    <?php endwhile; ?>
                                    <tr>
                                    	<td colspan="2"><a href="./forums/index.php?showforum=5">More</a></td>
                                    </tr>
                                </tbody>
                            </table>
                            
                        </div>
                        <div class="tab-pane" id="pc">
                        
                        	<!-- Table PC -->
                            <table class="table">
                            	<thead>
                                	<tr>                                        
                                        <th>Thread</th>
                                        <th>Discuss</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php while($forumData = mysqli_fetch_array($DisPCQuery)): ?>
                                        <tr>
                                            <td><a href="./forums/index.php?/topic/<?php echo $forumData['tid']; ?>-<?php echo $forumData['title_seo']; ?>"><?php echo $forumData['title']; ?></a></td>
                                            <td><?php echo number_format($forumData['posts']);  ?></td>
                                        </tr>
                                    <?php endwhile; ?>
                                    <tr>
                                    	<td colspan="2"><a href="./forums/index.php?showforum=5">More</a></td>
                                    </tr>
                                </tbody>
                            </table>
                            
                        </div>
                        <div class="tab-pane" id="online">
                        
                        	<!-- Table Online -->
                            <table class="table">
                            	<thead>
                                	<tr>                                        
                                        <th>Thread</th>
                                        <th>Discuss</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php while($forumData = mysqli_fetch_array($DisOnlineQuery)): ?>
                                        <tr>
                                            <td><a href="./forums/index.php?/topic/<?php echo $forumData['tid']; ?>-<?php echo $forumData['title_seo']; ?>"><?php echo $forumData['title']; ?></a></td>
                                            <td><?php echo number_format($forumData['posts']);  ?></td>
                                        </tr>
                                    <?php endwhile; ?>
                                    <tr>
                                    	<td colspan="2"><a href="./forums/index.php?showforum=5">More</a></td>
                                    </tr>
                                </tbody>
                            </table>
                            
                        </div>
                        <div class="tab-pane" id="handheld">
                        
                        	<!-- Table Handheld -->
                            <table class="table">
                            	<thead>
                                	<tr>                                        
                                        <th>Thread</th>
                                        <th>Discuss</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php while($forumData = mysqli_fetch_array($DisHandQuery)): ?>
                                        <tr>
                                            <td><a href="./forums/index.php?/topic/<?php echo $forumData['tid']; ?>-<?php echo $forumData['title_seo']; ?>"><?php echo $forumData['title']; ?></a></td>
                                            <td><?php echo number_format($forumData['posts']);  ?></td>
                                        </tr>
                                    <?php endwhile; ?>
                                    <tr>
                                    	<td colspan="2"><a href="./forums/index.php?showforum=5">More</a></td>
                                    </tr>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                    
                </div>
				<!-- End Forum Listings -->
                
            </div>
            <!-- End Left Column -->
              
            <!-- Begin Right Column -->
            <div class="col-md-3">
                
                <?php include_once('./navigation-side.php'); ?>
            
            </div>
            <!-- End Right Column -->
        
        </div>
    
    </div>
    <!-- End Main Container -->
    
    <footer>
        
        <div class="col-lg-8">
        
            <div class="container">
            
            	<?php include_once('./footer-links.php'); ?>
                
                <div class="panel panel-default">
                
                    <div class="panel-body">
                    
                    <h2>Welcome to Gamerz Unite <strong>Gaming Community</strong>!</h2>
        
                    <!--<p>Our site is dedicated to uniting gamers around the world and giving them a place to find lan parties, discuss their favorite games, gaming strategies, 
                    find and join clans or guilds, <a href="./play-free-games.php" style="text-decoration: none;">play free games</a>, read gaming news, apply 
                    for <a href="./forums/index.php?showtopic=15" style="text-decoration: none;">game web site hosting</a> and discuss relevant game topics as well as game 
                    issues that affect all gamers. </p>-->
                    
                    <p>Our site is dedicated to uniting gamers around the world and giving them a place to find  and post lan parties, discuss their favorite games and check out the latest
                    gaming articles or meme. <!--whether it be discussing 
                    the latest winner on the <a href="http://www.jackpotjoy.com/viewgames/deal-or-no-deal" target="_blank" style="text-decoration: none;">deal or no deal game</a> or how Mario Brothers is the 
                    best game ever-->. We also offer gaming strategies, find and join clans or guilds, <a href="./play-free-games.php" style="text-decoration: none;">play free games</a>, 
                    read gaming news, apply for <a href="./forums/index.php?showtopic=15" style="text-decoration: none;">game web site hosting</a> and discuss relevant game 
                    topics as well as game issues that affect all gamers.</p>
                    
                    <p>Lan parties are also a great way for gamers world wide to unite and discuss their experiences whether you play 
                    <a href="./forums/index.php?showforum=5" style="text-decoration: none;">lan games</a> 
                    <!--<a href="http://www.windowscasino.com/canada/free-slots/" target="_blank" style="text-decoration: none;">free slots at windows casino</a> games -->
                    or you just simply like gaming casually at home with friends.</p>

                    <p class="Margins">We provide Gaming News, Lan Party Directory, Gaming Clan Site Hosting for Free and Online Games to play. Click on 
                    the Lan Events link above to start your search of our directory of Lan Parties &amp; other Gaming Events. If your state or city is not 
                    listed in our menu above then tell us and we'll be sure to add it to our list.<!--, or for a better optimized search, 
                    discover <a href="http://onlinecasinoproject.com" target="_blank" style="text-decoration: none;">online casino reviews</a> while you play at the comfort of your home.--></p>
                    
                    <!--<p>Learn more about <a href="./bingo_and_gaming.php" title="Bingo and Gaming">Bingo and Gaming</a>.</p>-->
                    </div>
                    
                </div>
                
                <?php include_once('./affiliates.php'); ?>
        		
                <?php include_once('./footer-copyright.php'); ?>
                
            </div>
        
        </div>
    
    </footer>        
        
    <?php include_once('./global-js.php'); ?>
    
	<script src="js/home.js"></script>
    
</body>

</html>