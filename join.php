<!DOCTYPE html>
<html lang="en">

<?php include_once('./includes/header.includes.php'); ?>

<head>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<meta name="keywords" content="Gamerz Unite, lan party, lan parties, lan party listing, lan party listings, lan events, lan gaming event, gaming event, gaming events, big lan parties,">
	
	<meta name="description" content="dynamic lan party listing here">
    
    <meta name="verify-v1" content="oIgbVOUJX3dUtEFWlowqGg3+TT9cQF/AJ6fLLRYjZII="> 
    
    <!-- Title Tag -->
	<title>Join Gamerz Unite | List your Lan Party | Post Gaming Event</title>
    
    <!-- Bootstrap -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/tb_overrides.css" rel="stylesheet">
    <link href="./css/tb_overrides_content.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <?php include_once('./google-analytics.php'); ?>

</head>

<body>

	<?php include_once('./navigation-main.php'); ?>
    
    <div class="container">
    	
        <div class="row inner">
        
        	<!-- Begin Right Column -->
        	<div class="col-md-9 inner">
                
                <div class="bg-primary hdr_push">
                    <h1>Join Gamerz Unite</h1>
                    <h2>Register your account below:</h2>
                </div>
                
                <!-- Begin Main Content -->
                <div class="col-lg-12">
                    
                    <p>Welcome to the <strong>Gamerz Unite</strong> sign up page. Please fill in the short form below in order to gain access to the Gamerz Unite forum, Lan Party Listings
                    and posting your own Gaming Event.</p>
                    
                    <style>
                    .input-group {
						margin: 25px 0;	
					}
                    </style>
                    
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="sizing-addon1">Username</span>
                        <input type="text" class="form-control" placeholder="Enter Username..." aria-describedby="sizing-addon1">
                    </div>
                    
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="sizing-addon1">E-Mail</span>
                        <input type="text" class="form-control" placeholder="Enter E-Mail..." aria-describedby="sizing-addon1">
                    </div>
                    
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="sizing-addon1">Password</span>
                        <input type="text" class="form-control" placeholder="Enter Password..." aria-describedby="sizing-addon1">
                    </div>
                    
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="sizing-addon1">Re-Type Password</span>
                        <input type="text" class="form-control" placeholder="Re-Enter Password..." aria-describedby="sizing-addon1">
                    </div>
                    
                    <div class="well">
                        <!-- Split button -->
                        <div class="btn-group">
                            <button type="button" class="btn btn-default">-- Select --</button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="btn-group btn-group-justified" role="group" aria-label="Join">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-primary">Join</button>
                        </div>
                    </div>
                    
                </div>
                <!-- End Main Content -->
            
            </div>
            <!-- End Left Column -->

			<!-- Begin Right Column -->
            <div class="col-md-3">
                
                <?php include_once('./navigation-side.php'); ?>
            
            </div>
            <!-- End Right Column -->
        
        </div>
    
    </div>
    <!-- End Main Container -->

	<?php include_once('./footer.php'); ?>
    
    <?php include_once('./global-js.php'); ?>
    
    specificjs
    
</body>

</html>