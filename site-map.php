<!DOCTYPE html>
<html lang="en">

<?php include_once('./includes/header.includes.php'); ?>

<head>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<meta name="keywords" content="Online Gaming Community, Online Gaming, Games, Gamers, Online Games, Gaming News, Lan Party, Lan Parties, 
    Lans, Lan, Gaming Events, Game Events" />
	
	<meta name="description" content="Gamerz Unite Site Map. Find your way through our site using this site map." />
    
    <meta name="verify-v1" content="oIgbVOUJX3dUtEFWlowqGg3+TT9cQF/AJ6fLLRYjZII=" /> 
    
    <!-- Title Tag -->
	<title>GamerzUnite Site Map | Lan Party Site Map</title>
    
    <!-- Bootstrap -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/tb_overrides.css" rel="stylesheet">
    <link href="./css/tb_overrides_content.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <?php include_once('./google-analytics.php'); ?>

</head>

<body>

	<?php include_once('./navigation-main.php'); ?>
    
    <div class="container">
    	
        <div class="row inner">
        
        	<!-- Begin Right Column -->
        	<div class="col-md-9 inner">
                
                <div class="bg-primary hdr_push">
                    <h1>GamerzUnite.com Site Map</h1>
                    <h2>Confused on where to go? Then use this nifty site map! :)</h2>
                </div>
                
                <!-- Begin Main Content -->
                <div class="col-lg-12">
                    
                    <p><a href="index.php" title="Gamerz Unite Home Page">Home</a></p>
                    <p>Gamerz Unite Home Page.</p>
                    
                    <p><a href="./forums/index.php?s=ca159dd1e9dc4745fb468db37545100b&amp;act=Reg&amp;CODE=00" title="Join the Gamerz Unite Gaming Community">Unite! (Join)</a></p>
                    <p>Join the Gamerz Unite Gaming Community to discuss Gaming related events with fellow gamers.</p>
                    
                    <p><a href="./forums/" title="Gamerz Unite Gaming Forums">Forums</a></p>
                    <p>Browse gaming forum threads and post about anything related to gaming here.</p>
                    
                    <p><a href="./forums/index.php?showforum=5" title="Browse Our Lan Party Directory or Post your Own Gaming Event!">Browse Lan / Events</a></p>
                    <p>Browse our Lan Party Directory, post your own Lan Party event or discuss Lan Parties with other Gamers.</p>
                    
                    <p><a href="./forums/index.php?showtopic=15&amp;pid=65&amp;st=0&amp;#entry65" title="Free Game / Clan Site Hosting">Free Game / Clan Site Hosting</a></p>
                    <p>Looking to have your Clan or Game related web site hosted for Free? View the Hosting Request rules here.</p>
                    
                    <p><a href="./online-games.php" title="Online Games, Old School Games">Online Games</a></p>
                    <p>Bored? Play Online Games here.</p>
                    
                    <p><a href="host-lan-party.php" title="Lan Games | Lan Party Games | Setup Lan Party | How to Lan Game">Tips on Hosting a Lan Party &amp; Finding Lan Games</a></p>
                    <p>Information on how to setup a lan party or search for the right Lan Games to play at your lan.</p>
                
                    <h3>Affiliates Expanded</h3>
                    
                    <p><a href="http://www.4webgames.com/?linkpartner=59" target="_blank">4WebGames.com</a> .
                    <a href="http://www.dmegs.com/" rel="nofollow">Dmegs Web Directory</a> .
                    <a href="http://www.the-games-hub.com/Video-Games/" title="directory of Video Games related websites." rel="nofollow">Video Games resources</a> .
                    <a href="http://allgameslist.com/?code=780" title="Your guide to games on the Internet.." rel="nofollow">All Games List</a> . 
                    <a href="http://www.trafficechoes.com/" title="Need Free Targeted Traffic? Variety of Advertising needs to choose from. It's Fun, Fast, Easy and Works!!" rel="nofollow">TrafficEchoes.com</a> .</p>
        
                    <p><a href="http://www.discussvideogames.com/" rel="nofollow" title="Video Game Reviews, Video Game Cheats">Video Game Reviews, Video Game Cheats</a><br />
                    DiscussVideoGames game reviews for all systems located in the video game forums. Join the community, post your walkthroughs, buy video games, rent games online, 
                    and download video game cheats!</p>
                    
                </div>
                <!-- End Main Content -->
            
            </div>
            <!-- End Left Column -->

			<!-- Begin Right Column -->
            <div class="col-md-3">
                
                <?php include_once('./navigation-side.php'); ?>
            
            </div>
            <!-- End Right Column -->
        
        </div>
    
    </div>
    <!-- End Main Container -->

	<?php include_once('./footer.php'); ?>
    
    <?php include_once('./global-js.php'); ?>
    
</body>

</html>