// FACEBOOK COMMENT FIX
function fbCommentsWorkaround() {      
	function resizeFacebookComments(){
		var src = $('.fb-comments iframe').attr('src').split('width='),
		width = $('.fb-comments').parent().parent().width();
		$('.fb-comments iframe').attr('src', src[0] + 'width=' + width);
		$('.fb-comments iframe').css({width: width});
		$('.fb-comments span').css({width: width});
	}

	FB.Event.subscribe('xfbml.render', resizeFacebookComments);
	
	$(window).on('resize', function(){
		resizeFacebookComments();
	});
	
	$('.fb-comments iframe').on('load', function() {
		resizeFacebookComments();
		$('.fb-comments iframe').unbind('load');
	});
}

window.fbAsyncInit = function() {
	FB.init({
		appId	: "596895303735715",
		status	: true,
		cookie	: true,
		oauth	: true,
		xfbml	: true,
		logging	: false
	});
    fbCommentsWorkaround();
};

(function(d){
	var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
	js = d.createElement('script'); js.id = id; js.async = true;
	js.src = "//connect.facebook.net/en_US/all.js";
	d.getElementsByTagName('head')[0].appendChild(js);
}(document));