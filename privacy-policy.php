<!DOCTYPE html>
<html lang="en">

<?php include_once('./includes/header.includes.php'); ?>

<head>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<meta name="keywords" content="Online Gaming Community, Online Gaming, Games, Gamers, Online Games, Gaming News, Lan Party, Lan Parties, 
    Lans, Lan, Gaming Events, Game Events, dallas lans, dallas lan parties, dfw lans, dfw lan parties" />
    
    <meta name="description" content="Gamerz Unite - How to host a Lan Party or Lan Event of your own!" />
    
    <meta name="verify-v1" content="oIgbVOUJX3dUtEFWlowqGg3+TT9cQF/AJ6fLLRYjZII=" /> 
    
    <!-- Title Tag -->
	<title>Gamerzunite.com Disclaimer</title>
    
    <!-- Bootstrap -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/tb_overrides.css" rel="stylesheet">
    <link href="./css/tb_overrides_content.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <?php include_once('./google-analytics.php'); ?>

</head>

<body>

	<?php include_once('./navigation-main.php'); ?>
    
    <div class="container">
    	
        <div class="row inner">
        
        	<!-- Begin Right Column -->
        	<div class="col-md-9 inner">
                
                <div class="bg-primary hdr_push">
                    <h1>Gamerzunite.com Privacy Policy</h1>
                    <h2></h2>
                </div>
                
                <!-- Begin Main Content -->
                <div class="col-lg-12">
                    
                    <p>Our Privacy Policy is designed to assist you in understanding how we collect and use the personal information you provide to us and to assist you in 
                    making informed decisions when using our site, products and services.</p>
                    
                    <p><strong>What Information Do We Collect?</strong></p>
                    
                    <p>When you visit our web site you may provide us with two types of information: personal information you knowingly choose to disclose that is collected on 
                    an individual basis and web site use information collected on an aggregate basis as you and others browse our Web site.</p>
                    
                    <p><strong>Personal Information You Choose to Provide</strong></p>
                    
                    <p><em>Registration Information</em></p>
                    
                    <p>You will provide us information about yourself when you register to be a member of gamerzunite.com, register for certain services, or register for email newsletters 
                    and alerts. You may also provide additional comments on how you see gamerzunite.com servicing your needs and interests.</p>
                    
                    <p><em>Email Information</em></p>
                    
                    <p>If you choose to correspond with us through email, we may retain the content of your email messages together with your email address and our responses.</p>
                    
                    <p><em>Web Site Use Information</em></p>
                    
                    <p>Similar to other commercial web sites, our web site utilizes a standard technology called "cookies" (see explanation below, "What Are Cookies?") and web 
                    server logs to collect information about how our web site is used. Information gathered through cookies and web server logs may include the date and time of visits, 
                    the pages viewed, time spent at our web site, and the web sites visited just before and just after our web site.  We, our advertisers and ad serving companies may 
                    also use small pieces of code called "web beacons" or "clear gifs" to determine which advertisements and promotions users have seen and how users responded to them.</p>
                    
                    <p><strong>How Do We Use the Information That You Provide to Us?</strong></p>
                    
                    Broadly speaking, we use personal information for purposes of administering and expanding our business activities, providing customer service and making available 
                    other products and services to our customers and prospective customers. Occasionally, we may also use the information we collect to notify you about important 
                    changes to our web site, new services and special offers we think you will find valuable. You may notify us at any time if you do not wish to receive these offers 
                    by contacting one of the site admins on the <a href="./forums">forum</a>.
                    
                    <p><strong>What Are Cookies?</strong></p>
                    
                    <p>A cookie is a very small text document, which often includes an anonymous unique identifier. When you visit a web site, that site's computer asks your computer 
                    for permission to store this file in a part of your hard drive specifically designated for cookies. Each Web site can send its own cookie to your browser if your 
                    browser's preferences allow it, but (to protect your privacy) your browser only permits a web site to access the cookies it has already sent to you, not the 
                    cookies sent to you by other sites.</p>
                    
                    <p><strong>How Do We Use Information We Collect from Cookies?</strong></p>
                    
                    <p>As you browse our web site, the site uses its cookies to differentiate you from other users so your member profile is only available to you. In some cases, we also 
                    use cookies to prevent you from seeing unnecessary advertisements or requiring you to log in more than is necessary for security. Cookies, in conjunction with our 
                    web server's log files, allow us to calculate the aggregate number of people visiting our web site and which parts of the site are most popular. This helps us gather 
                    feedback in order to constantly improve our web site and better serve our users. Cookies do not allow us to gather any personal information about you and we do 
                    not generally store any personal information that you provided to us in your cookies.</p>
                    
                    <p><strong>Sharing Information with Third Parties</strong></p>
                    
                    <p>We may enter into alliances, partnerships or other business arrangements with third parties who may be given access to personal information including your name, 
                    address, telephone number and email for the purpose of providing you information regarding products and services that we think will be of interest to you. In connection 
                    with alliances, partnerships or arrangements, we may also provide certain information to third parties if we have determined that the information will be used in a 
                    responsible manner by a responsible third party. For example, some of our partners operate other online gaming sites or provide services on our site, while others 
                    power offerings developed by us for your use. In connection with these offerings and business operations, our partners and other third parties may have access to 
                    your personal information for use in connection with business activities. As we develop our business, we may buy or sell assets or business offerings. Customer, 
                    email, and visitor information is generally one of the transferred business assets in these types of transactions. We may also transfer such information in the 
                    course of corporate divestitures, mergers, or any dissolution.</p>
                    
                    <p><strong>Notice of New Services and Changes</strong></p>
                    
                    <p>Occasionally, we may also use the information we collect to notify you about important changes to our web site, new services and special offers we think you 
                    will find valuable. As our customer, you will be given the opportunity to notify us of your desire not to receive these offers by contacting one of the site 
                    admins on the <a href="./forums">forum</a>. </p>
                        
                </div>
                <!-- End Main Content -->
            
            </div>
            <!-- End Left Column -->

			<!-- Begin Right Column -->
            <div class="col-md-3">
                
                <?php include_once('./navigation-side.php'); ?>
            
            </div>
            <!-- End Right Column -->
        
        </div>
    
    </div>
    <!-- End Main Container -->

	<?php include_once('./footer.php'); ?>
    
    <?php include_once('./global-js.php'); ?>
    
</body>

</html>