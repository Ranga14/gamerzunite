<?php
	include_once('./includes/dbconnect.inc.php');
	
	function friendlyURL($inputString) {
		$url = strtolower($inputString);
		$patterns = $replacements = array();
		$patterns[0] = '/(&amp;|&)/i';
		$replacements[0] = '-and-';
		$patterns[1] = '/[^a-zA-Z01-9]/i';
		$replacements[1] = '-';
		$patterns[2] = '/(-+)/i';
		$replacements[2] = '-';
		$patterns[3] = '/(-$|^-)/i';
		$replacements[3] = '';
		$url = preg_replace($patterns, $replacements, $url);
		
		return $url;
	}
	
	// Pull News Thread Data
	$Article			= "SELECT tid,title,title_seo,topic_firstpost FROM ibf_topics WHERE forum_id='76' AND approved='1' ORDER BY start_date DESC LIMIT 5";
	$ArticleQuery		= mysqli_query($mysqli, $Article);
?>