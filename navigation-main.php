	<div id="fb-root"></div>
	<script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    
    <script>
        window.fbAsyncInit = function() {
        FB.init({
          appId      : '1651976775039775',
          xfbml      : true,
          version    : 'v2.4'
        });
        };

        (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <nav class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
    
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="./index.php"><img src="./img/logo_new_no_sticks_small.png" class="img-responsive logo" alt="Online Gaming Community | Lan Parties | Lan Party Directory | Play Free Games"></a>
            </div>
    
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav nav_first">
                	<li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Menu <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="./forums/">Forums</a></li>
                            <li class="divider"></li>
                            <li><a href="./forums/index.php?showforum=5">Browse Lan Parties</a></li>
                            <li><a href="./forums/index.php?showtopic=4">List your Lan Party</a></li>
                            <li class="divider"></li>
                            <li><a href="./forums/index.php?showtopic=15&amp;pid=65&amp;st=0&amp;#entry65">Free Game Site Hosting</a></li>
                            <li><a href="./play-free-games.php">Free Games List</a></li>
                            <li><a href="./host-lan-party.php">How to Host a Lan Party</a></li>
                            <li><a href="./your-guide-to-modern-lan-compatible-games">Modern Lan Party Games List</a></li>
                            <!--<li><a href="./game-info-and-history.php">Game Info</a></li>-->
                        </ul>
                    </li>
                    <li class="active"><a href="./forums/index.php?app=core&amp;module=global&amp;section=register">Join GZU</a></li>
                    <li><p class="navbar-text">Latest Lan Party:</p></li>
                    <li class="recent_lans">
                        <ul class="newsticker">
							<?php
							// Pull Lan (U.S.) Data
							$lanRecent			= "SELECT tid,title,posts,forum_id FROM ibf_topics 
												 WHERE forum_id='5' OR forum_id='6' OR forum_id='7' OR forum_id='8' OR forum_id='37' OR forum_id='9' OR forum_id='11' OR forum_id='12' 
												 OR forum_id='13' OR forum_id='46'
												 OR forum_id='40' OR forum_id='41' OR forum_id='42' OR forum_id='43' OR forum_id='44' OR forum_id='45' OR forum_id='68'
												 OR forum_id='70' OR forum_id='69' OR forum_id='71' OR forum_id='72' OR forum_id='78' OR forum_id='80' OR forum_id='87'
												 OR forum_id='88' OR forum_id='89'
												 ORDER BY start_date DESC LIMIT 5";
							$lanRecentQuery		= mysqli_query($mysqli, $lanRecent);
							while($lanRecentData= mysqli_fetch_array($lanRecentQuery)): ?>
                                <li class="recent_long"><a href="./forums/index.php?showtopic=<?php echo $lanRecentData['tid']; ?>"><?php echo $lanRecentData['title']; ?></a></li>
                                <li class="recent_short"><a href="./forums/index.php?showtopic=<?php echo $lanRecentData['tid']; ?>"><?php echo substr($lanRecentData['title'], 0, 30); ?>...</a></li>
                            <?php endwhile; ?>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="http://www.facebook.com/lanparties"><img src="./img/facebook.png" alt="Like us on Facebook."></a></li>
                    <li><a href="http://www.twitter.com/lanparties"><img src="./img/twitter.png" alt="Follow us on Twitter."></a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>