                <!--<a href="./forums/index.php?app=core&module=global&section=register" class="join_side"><img src="./graphics/btn_join-gzu.jpg" border="0" alt="Join GamerzUnite and Unite with other Gamerz." /></a>
                -->
                <div class="bg-info">
                	<!--A Piece of Our Mind-->
                    <img src="./img/article_title.png" class="img-responsive" alt="A Piece of Our Mind">
                </div>
                
                <?php while($data = mysqli_fetch_assoc($ArticleQuery)):
				
					$url = $data['title'];

					$aImg		= "SELECT attach_id,attach_thumb_location,attach_location FROM ibf_attachments WHERE attach_rel_id='$data[topic_firstpost]'";
					$aImgQuery	= mysqli_query($mysqli, $aImg);
					$aImgData	= mysqli_fetch_array($aImgQuery);
					
					?>
                    
                    <style>
                    	.panel_bg<?php echo $aImgData['attach_id']; ?> { background: url('./forums/uploads/<?php echo $aImgData['attach_thumb_location']; ?>') top left no-repeat; }
						/* If screen resolution is no larger, display full width image. */
						@media (max-width: 991px) {
							.panel_bg<?php echo $aImgData['attach_id']; ?> { background: url('./forums/uploads/<?php echo $aImgData['attach_location']; ?>') top left no-repeat; }
						}
                    </style>
			
					<a class="panel panel-default panel_bg<?php echo $aImgData['attach_id']; ?>" href="./<?php echo $data['title_seo']; ?>">
						<div class="panel-body">
							<h3 class="panel-title"><?php echo substr($data['title'], 0, 29); ?>...</h3>
						</div>
					</a>
                
                <?php endwhile; ?>
                
                <a class="panel panel-default" href="./game-articles-oped-previews-reviews.php">
                    <div class="panel-body">
                        <h3 class="panel-title" style="margin-top: 0; background-color: #1a2224;">Read More Articles...</h3>
                    </div>
                </a>
                
                <div class="col-lg-6">
                
                	<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=3153070" class="donate"><img src="./img/donate.png" class="img-responsive" alt="Donate to GamerzUnite"></a>
                    
                    <div class="input-group">
                        
                        <form action="search.php" id="cse-search-box">
                            <input type="hidden" name="cx" value="partner-pub-7646919961525485:qei84y-57ac" />
                            <input type="hidden" name="cof" value="FORID:11" />
                            <input type="hidden" name="ie" value="UTF-8" />
                            
                            <input type="text" name="q" class="form-control search-gzu" value="Search GZU">
                            <span class="input-group-btn">
                                <button name="sa" class="btn btn-default" type="submit">Go!</button>
                            </span>
                        </form>
                     
                    	<script type="text/javascript" src="http://www.google.com/coop/cse/brand?form=cse-search-box&amp;lang=en"></script>
                        
                    </div><!-- /input-group -->
                <?php
				// Google PII Fix
				if (empty($_GET['utm_content'])) {
				?>
                    <div class="google_250">
                    	
                        <script async src="http://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        
                        <!-- GZU250 -->
                        <ins class="adsbygoogle"
                             style="width: 250px; height: 250px;"
                             data-ad-client="ca-pub-7646919961525485"
                             data-ad-slot="8179718803"></ins>
                        <script>
                        	(adsbygoogle = window.adsbygoogle || []).push({});
                    	</script>  
                    
                    </div>
                    
                    <div class="google_200">
                        
                        <script type="text/javascript"><!--
                            google_ad_client = "pub-7646919961525485";
                            /* 200x200, created 2/8/09 */
                            google_ad_slot = "4285315365";
                            google_ad_width = 200;
                            google_ad_height = 200;
                            //-->
                        </script>
                        
                        <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>
                    
                    </div>
				<?php } ?>
                </div><!-- /.col-lg-6 -->
                
                <div class="fb_contain">
                    <div class="fb-like-box" data-href="http://www.facebook.com/lanparties" data-width="720" data-height="560" data-show-faces="true" data-stream="true" data-header="false"></div>
                </div>