<!DOCTYPE html>
<html lang="en">

<?php
	include_once('./includes/header.includes.php');
	
	//$urldata = str_replace('-', ' ', $_GET['showtopic']);
	
	// Pull News Thread Data
	$Thread				= "SELECT tid,title,title_seo,description,starter_name,starter_id,start_date,posts,forum_id,topic_firstpost FROM ibf_topics WHERE title_seo='$_GET[showtopic]' AND approved='1'";
	$ThreadQuery		= mysqli_query($mysqli, $Thread);
	$ThreadCheck		= mysqli_num_rows($ThreadQuery);
	$ThreadData			= mysqli_fetch_array($ThreadQuery);
	
	// Pull Large Image for Article
	$ContentImg			= "SELECT attach_location FROM ibf_attachments WHERE attach_rel_id='$ThreadData[topic_firstpost]'";
	$ContentImgQuery	= mysqli_query($mysqli, $ContentImg);
	$ContentImg			= mysqli_fetch_array($ContentImgQuery);
	
	// Pull Authors Avatar
	$avatar				= "SELECT pp_thumb_photo FROM ibf_profile_portal WHERE pp_member_id='$ThreadData[starter_id]'";
	$avatarQuery		= mysqli_query($mysqli, $avatar);
	$avatarData			= mysqli_fetch_array($avatarQuery);
					
	$Post				= "SELECT pid,author_id,author_name,post_date,post FROM ibf_posts WHERE topic_id='$ThreadData[tid]'";
	$PostQuery			= mysqli_query($mysqli, $Post);
	$PostCount			= mysqli_num_rows($PostQuery);
	$PostData			= mysqli_fetch_array($PostQuery);

	// Pull Post Data
    $PostData = $PostData['post'];
	
	// Convert odd colon issue
	$colon = str_replace("&#58;", ":", $PostData);
	
	// Convert Bolding Tags to HTML
	$msg1 = str_replace("[b]", "<b>", $colon);
	$msg2 = str_replace("[/b]", "</b>", $msg1);
	
	// Convert Italtics Tags to HTML
	$msg3 = str_replace("[i]", "<em>", $msg2);
	$msg4 = str_replace("[/i]", "</em>", $msg3);
	
	// Convert Quote Tags to HTML
	$quote1 = str_replace("[quote]", "<blockquote>", $msg4);
	$quote2 = str_replace("[/quote]", "</blockquote>", $quote1);
	
	// Convert Img Tags to HTML
	$img = preg_replace( '/\[img]([^\[]+)\[\/img\]/', '<img src="$1" />', $quote2);
	
	//$url = preg_replace( '/\[url=([^\]]+)\]([^\[]+)\[\/url\]/', '<a href="$1" target="_blank">$2</a>', $img);
	
	// Convert IP URL to normal URL
	$url = str_replace('[url=&quot;', '<a href=&quot;', $img);
	$url2 = str_replace('&quot;]', '&quot; target=&quot;_blank&quot;>', $url);
	$url_final = str_replace('[/url]', '</a>', $url2);
	
	// Gotta strip out those quote tags now...
	$quoted = str_replace('&quot;', '"', $url_final);
	
	// Convert Youtube URL to Video Tag
	$youtube = str_replace('[media]http://www.youtube.com/watch?v=', '[media]http://www.youtube.com/v/', $quoted);
	$youtube2 = str_replace('[/media]', '[/media]', $youtube);
	
	// Convert HR's
	$hrtag = str_replace("[hr]", "<hr>", $youtube2);
	
	// Replace IP Media Tags w/Youtube HTML
	$youtube_final = preg_replace( '/\[media]([^\[]+)\[\/media\]/', '<object type="application/x-shockwave-flash" height="433" width="720" data="$1"><param name="movie" value="$1"><param name="allowScriptAccess" value="sameDomain"><param name="quality" value="best"><param name="bgcolor" value="#FFFFFF"><param name="scale" value="noScale"><param name="salign" value="TL"><param name="FlashVars" value="playerMode=embedded" /><param name="wmode" value="transparent"/></object>', $hrtag);
	
	// Final Output
	$post_converted = $youtube_final;
	
	// Convert to Proper H1 Title
	$title_percent = str_replace('%', '<span>%</span>', $ThreadData['title']);
	$title_quote = str_replace('&#34;', '<span>"</span>', $title_percent);
	$title_dollar = str_replace('&#036;', '<span>&#036;</span>', $title_quote);
	$title_and = str_replace('&#38;', '<span>&#38;</span>', $title_dollar);
	$titleFinal = str_replace('-', '<span>-</span>', $title_and);
	
	// Convert to Proper Subtitle Title
	$sub_title_percent = str_replace('%', '<span>%</span>', $ThreadData['description']);
	$sub_title_quote = str_replace('"', '<span>"</span>', $sub_title_percent);
	$sub_title_dollar = str_replace('$', '<span>&#036;</span>', $sub_title_quote);
	$sub_title_and = str_replace('&', '<span>&#38;</span>', $sub_title_dollar);
	$sub_title_equals = str_replace('=', '<span>&#61;</span>', $sub_title_and);
	$subTitleFinal = str_replace('-', '<span>-</span>', $sub_title_equals);
	
	if ($ThreadData['tid'] == 301)
	{
		header("Location: http://www.gamerzunite.com/Evolution-of-the-Halo-Sniper-Rifle");
	}
?>

<head>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<meta name="keywords" content="<?php if ($ThreadCheck > 0) { echo $ThreadData['title']; } else { echo "Page Cannot be Found!"; } ?>, games news, gaming news, game previews, game reviews, game opinions" />
	
	<meta name="description" content="<?php if ($ThreadCheck > 0) { echo $ThreadData['description']; } else { echo "Page Cannot be Found!"; } ?>" />
    
    <meta name="verify-v1" content="oIgbVOUJX3dUtEFWlowqGg3+TT9cQF/AJ6fLLRYjZII=" /> 
    
    <!-- Title Tag -->
	<title><?php if ($ThreadCheck > 0) { echo $ThreadData['title']; } else { echo "Page Cannot be Found!"; } ?> | Gaming News | Gamerzunite.com</title>
    
    <!-- Bootstrap -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/tb_overrides.css" rel="stylesheet">
    <link href="./css/tb_overrides_content.css" rel="stylesheet">
    
    <!-- FB Image Scrap -->
    <!-- <link rel="image_src" href="http://www.gamerzunite.com/forums/uploads/<?php //echo $ContentImg['attach_location']; ?>"> -->

    <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
    <meta property="fb:app_id" content="1651976775039775" /> 
    <meta property="og:type"   content="website" /> 
    <meta property="og:url"    content="http://www.gamerzunite.com/<?php echo $ThreadData['title_seo']; ?>" /> 
    <meta property="og:title"  content="<?php if ($ThreadCheck > 0) { echo $ThreadData['title']; } else { echo "Page Cannot be Found!"; } ?>" /> 
    <meta property="og:image"  content="http://www.gamerzunite.com/forums/uploads/<?php echo $ContentImg['attach_location']; ?>" /> 
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <?php include_once('./google-analytics.php'); ?>

</head>

<body>

	<?php include_once('./navigation-main.php'); ?>
    
    <div class="container">
    	
        <div class="row inner">
        
        	<!-- Begin Right Column -->
        	<div class="col-md-9 inner">
            	
                <?php if ($ThreadCheck > 0): ?>
                    
                    <img src="./forums/uploads/<?php echo $ContentImg['attach_location']; ?>" class="img-responsive" alt="<?php echo $ThreadData['title']; ?>">
                    
                    <div class="bg-primary">
                        <h1><?php echo $titleFinal; ?></h1>
                        <h2><?php echo $subTitleFinal; ?></h2>
                    </div>
                    
                    <!-- Begin Main Content -->
                    <div class="col-lg-12">
                    
                        <div class="row">
                        
                            <div class="cn_left">
                                <img src="./forums/uploads/<?php echo $avatarData['pp_thumb_photo']; ?>" alt="<?php echo $ThreadData['starter_name']; ?>" class="img-rounded"> 
                                by <a href="./forums/index.php?showuser=<?php echo $ThreadData['starter_id']; ?>"><?php echo $ThreadData['starter_name']; ?></a> 
                                on <?php $postDate = strftime("%b %d, %Y @ %I:%M %p", $ThreadData['start_date']); echo "$postDate"; ?>
                            </div>
                            
                            <div class="cn_right">
                            	
                                <a href="#comments" class="comments_lnk">Comment(s)</a>
                                
                                <!-- AddThis Button BEGIN -->
                                <div class="addthis_toolbox addthis_default_style ">
                                <a class="addthis_button_preferred_1"></a>
                                <a class="addthis_button_preferred_2"></a>
                                <a class="addthis_button_preferred_3"></a>
                                <a class="addthis_button_preferred_4"></a>
                                <a class="addthis_button_compact"></a>
                                <a class="addthis_counter addthis_bubble_style"></a>
                                </div>
                                <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
                                <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ranga14"></script>
                                <!-- AddThis Button END -->
                                
                            </div>
                            
                        </div>
                        
						<?php
                        // Google PII Fix
                        if (empty($_GET['utm_content'])) {
                        ?>
                            <div class="g_728">
                                <script async src="http://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                <!-- 728x15, created 2/9/09 -->
                                <ins class="adsbygoogle" style="display:inline-block;width:728px;height:15px" data-ad-client="ca-pub-7646919961525485" data-ad-slot="9740161256"></ins>
                                <script> (adsbygoogle = window.adsbygoogle || []).push({}); </script>
                            </div>
                            <div class="g_468">
                            	<script async src="http://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                <!-- GU Content (Top) 720 -->
                                <ins class="adsbygoogle"
                                     style="display:inline-block;width:468px;height:60px"
                                     data-ad-client="ca-pub-7646919961525485"
                                     data-ad-slot="2400397608"></ins>
                                <script>
                                (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>
                            </div>
                            <div class="g_320">
                            	<script async src="http://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                <!-- GU Content (Top) 320 -->
                                <ins class="adsbygoogle"
                                     style="display:inline-block;width:320px;height:50px"
                                     data-ad-client="ca-pub-7646919961525485"
                                     data-ad-slot="5353864000"></ins>
                                <script>
                                (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>
                            </div>
                        <?php } ?>
                        
                        <div class="post">
							<?php echo $post_converted; ?>
                        </div>
                        
                        <div class="g_728">
				<?php
				// Google PII Fix
				if (empty($_GET['utm_content'])) {
				?>
					<script async src="http://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- GZU-728-CONTENT -->
					<ins class="adsbygoogle"
					     style="display:inline-block;width:728px;height:90px"
					     data-ad-client="ca-pub-7646919961525485"
					     data-ad-slot="7840348005"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
			    <?php } ?>
                        </div>
                        
                        <div class="row comments">
                            
                            <a name="comments"></a>
                            <h3>Comments</h3>
                            
							<?php //$urlcdata = str_replace(' ', '-', $data['title']); ?>
                            <div class="fb-comments" data-href="http://www.gamerzunite.com/<?php echo $_GET['showtopic']; ?>" data-num-posts="5"></div>
                            
                            <a rel="nofollow" class="linkForum" href="./forums/index.php?/topic/<?php echo $ThreadData['tid']; ?>-<?php echo $_GET['showtopic']; ?>">Comment on this 
                            Article in our Forum</a>
                            
                            <h3 class="moreNews">More GamerzUnite News</h3>
                            
                            <div class="contain">
                            
								<?php 
                                // Pull News Thread Data
                                $News			= "SELECT tid,title,title_seo,description,posts,start_date,topic_firstpost FROM ibf_topics WHERE forum_id='18' AND approved='1' ORDER BY start_date DESC LIMIT 6";
                                $NewsQuery		= mysqli_query($mysqli, $News);
                                while($data 	= mysqli_fetch_assoc($NewsQuery)): 
                                    
                                    $url = $data['title'];
                                    
                                    // Pull Thumb Image for Article
                                    $NewsImg			= "SELECT attach_thumb_location FROM ibf_attachments WHERE attach_rel_id='$data[topic_firstpost]'";
                                    $NewsImgQuery		= mysqli_query($mysqli, $NewsImg);
                                    $NewsImg			= mysqli_fetch_array($NewsImgQuery);
                                    ?>
                                    
                                    <div class="col-sm-6 col-md-4">
                                        <div class="thumbnail">
                                            <a href="./<?php echo $data['title_seo']; ?>"><img data-src="holder.js/300x200" src="./forums/uploads/<?php echo $NewsImg['attach_thumb_location']; ?>" alt="<?php echo $data['title']; ?>"></a>
                                            <div class="caption">
                                                <h3><a href="./<?php echo $data['title_seo']; ?>"><?php echo $data['title']; ?></a></h3>
                                                <p><?php echo $data['description']; ?></p>
                                            </div>
                                        </div>
                                        <div class="btn_grey_box" style="background-color: #dbdbdb;"><?php $postDate = strftime("%B %#d @ %I:%M %p", $data['start_date']); echo "$postDate"; ?></div>
                                    </div>
                                
                                <?php endwhile; ?>
                                
                            </div>
                            
                        </div>                    
                        
                    </div>
                    <!-- End Main Content -->
                    
				<?php else: ?>
                
                    <div class="bg-primary" style="padding: 50px 0 12px 0;">
                        <h1>Page Cannot be Found!</h1>
                    </div>
                    
                    <!-- Begin Main Content -->
                    <div class="col-lg-12">
                    
                        <p>You have typed in an incorrect title. To see a list of exclusive GZU content, return to the home page:</p>
                        
                        <p><a href="./index.php">www.gamerzunite.com</a></p>
                        
                    </div>
                    <!-- End Main Content -->
                    
                <?php endif; ?>
                
            </div>
            <!-- End Left Column -->

			<!-- Begin Right Column -->
            <div class="col-md-3">
                
                <?php include_once('./navigation-side.php'); ?>
            
            </div>
            <!-- End Right Column -->
        
        </div>
    
    </div>
    <!-- End Main Container -->

	<?php include_once('./footer.php'); ?>
    
    <?php include_once('./global-js.php'); ?>
    
    <script src="js/video.js"></script>
    <script src="js/social.js"></script>
    
</body>

</html>