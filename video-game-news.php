<!DOCTYPE html>
<html lang="en">

<?php include_once('./includes/header.includes.php'); 

	// Pull News Thread Data
	//$Thread				= "SELECT tid,title,title_seo,description,posts,start_date,topic_firstpost FROM ibf_topics WHERE forum_id='18' AND approved='1' ORDER BY start_date DESC LIMIT 27";
	//$ThreadQuery		= mysqli_query($mysqli, $Thread);
?>

<head>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<meta name="keywords" content="Gamerz Unite, video game news, gaming news, latest games news, news gaming, gaming industry news, video game stories, video game news blog">
	
	<meta name="description" content="Catch up on the latest news in the video game industry.">
    
    <meta name="verify-v1" content="oIgbVOUJX3dUtEFWlowqGg3+TT9cQF/AJ6fLLRYjZII="> 
    
    <!-- Title Tag -->
	<title>Video Game News | Gaming News | Latest Games News</title>
    
    <!-- Bootstrap -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/tb_overrides.css" rel="stylesheet">
    <link href="./css/tb_overrides_content.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <?php include_once('./google-analytics.php'); ?>

</head>

<body>

	<?php include_once('./navigation-main.php'); ?>
    
    <div class="container">
    	
        <div class="row inner innerDark">
        
        	<!-- Begin Right Column -->
        	<div class="col-md-9 inner innerDark">
                
                <div class="bg-primary hdr_push">
                    <h1>GamerzUnite Gaming News</h1>
                    <h2>Not your regular ole video game news</h2>
                </div>
                
                <!-- Begin Main Content -->
                <div class="col-lg-12">
                    
                    <?php
					// This first query is just to get the total count of rows
					$sql = "SELECT COUNT(tid) FROM ibf_topics WHERE forum_id='18' AND approved='1' ORDER BY start_date";
					$query = mysqli_query($mysqli, $sql);
					$row = mysqli_fetch_row($query); 
					
					// Here we have the total row count 
					$rows = $row[0]; 
					
					// This is the number of results we want displayed per page 
					$page_rows = 27; 
					
					// This tells us the page number of our last page 
					$last = ceil($rows/$page_rows); 
					
					// This makes sure $last cannot be less than 1 
					if($last < 1) {
						$last = 1;
					} 
					
					// Establish the $pagenum variable
					$pagenum = 1; 
					
					// Get pagenum from URL vars if it is present, else it is = 1 
					if(isset($_GET['pn'])) {
						$pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
					} 
					
					// This makes sure the page number isn't below 1, or more than our $last page 
					if ($pagenum < 1) {
						$pagenum = 1;
					} else if ($pagenum > $last) { 
						$pagenum = $last;
					} 
					
					// This sets the range of rows to query for the chosen $pagenum 
					$limit = 'LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows; 
					
					// This is your query again, it is for grabbing just one page worth of rows by applying $limit 			
					$sql = "SELECT tid,title,title_seo,description,posts,start_date,topic_firstpost FROM ibf_topics WHERE forum_id='18' AND approved='1' ORDER BY start_date DESC $limit";
					$query = mysqli_query($mysqli, $sql); 
					
					// This shows the user what page they are on, and the total number of pages 
					$textline1 = "(<strong>$rows</strong>)";
					$textline2 = "Page <strong>$pagenum</strong> of <strong>$last</strong>"; 
					
					// Establish the $paginationCtrls variable 
					$paginationCtrls = ''; 
					
					// If there is more than 1 page worth of results 
					if($last != 1) { 
						
						/* First we check if we are on page one. If we are then we don't need a link to the previous page or the first page so we do nothing. 
						If we aren't then we generate links to the first page, and to the previous page. */ 
					
						if ($pagenum > 1) {
							
							$previous = $pagenum - 1; $paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$previous.'">Previous</a></li>'; 
						
							// Render clickable number links that should appear on the left of the target page number 
							for($i = $pagenum-4; $i < $pagenum; $i++) {
								if($i > 0) {
									$paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'">'.$i.'</a></li>'; 
								}
							}
						} 
						
						// Render the target page number, but without it being a link 
						$paginationCtrls .= '<li class="active"><a href="#">'.$pagenum.'</a><li>';
						
						// Render clickable number links that should appear on the right of the target page number 
						for($i = $pagenum+1; $i <= $last; $i++) {
							
							$paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'">'.$i.'</a></li>';
							
							if($i >= $pagenum+4) {
								break;
							}
						} 
						
						// This does the same as above, only checking if we are on the last page, and then generating the "Next" 
						if ($pagenum != $last) {
							$next = $pagenum + 1; $paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$next.'">Next</a></li>';
						}
						
					} 
					// Close your database connection mysqli_close($mysqli);
					?> 
                    
                    <?php while($data = mysqli_fetch_assoc($query)): 
                            
						$url = $data['title'];
						
						// Pull Thumb Image for Article
						$NewsImg			= "SELECT attach_thumb_location FROM ibf_attachments WHERE attach_rel_id='$data[topic_firstpost]'";
						$NewsImgQuery		= mysqli_query($mysqli, $NewsImg);
						$NewsImg			= mysqli_fetch_array($NewsImgQuery);
						?>
						
						<div class="col-sm-6 col-md-4">
							<div class="thumbnail">
								<a href="./<?php echo $data['title_seo']; ?>"><img data-src="holder.js/300x200" src="./forums/uploads/<?php echo $NewsImg['attach_thumb_location']; ?>" alt="<?php echo $data['title']; ?>"></a>
								<div class="caption">
									<h3><a href="./<?php echo $data['title_seo']; ?>"><?php echo $data['title']; ?></a></h3>
									<p><?php echo $data['description']; ?></p>
								</div>
							</div>
                            <div class="btn_grey_box"><?php $postDate = strftime("%B %#d @ %I:%M %p", $data['start_date']); echo "$postDate"; ?></div>
						</div>
					
					<?php endwhile; ?>
                    
                    <ul class="pagination pagination-large">
                        <?php echo $paginationCtrls; ?>
                    </ul>
				
                </div>
                <!-- End Main Content -->
            
            </div>
            <!-- End Left Column -->

			<!-- Begin Right Column -->
            <div class="col-md-3">
                
                <?php include_once('./navigation-side.php'); ?>
            
            </div>
            <!-- End Right Column -->
        
        </div>
    
    </div>
    <!-- End Main Container -->

	<?php include_once('./footer.php'); ?>
    
    <?php include_once('./global-js.php'); ?>
    
</body>

</html>