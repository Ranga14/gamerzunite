<!DOCTYPE html>
<html lang="en">

<?php include_once('./includes/header.includes.php'); ?>

<head>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<meta name="keywords" content="Online Gaming Community, Online Gaming, Games, Gamers, Online Games, Gaming News, Lan Party, Lan Parties, 
    Lans, Lan, Gaming Events, Game Events, dallas lans, dallas lan parties, dfw lans, dfw lan parties, play free games" />
    
    <meta name="description" content="Gamerz Unite - How to host a Lan Party or Lan Event of your own!" />
    
    <meta name="verify-v1" content="oIgbVOUJX3dUtEFWlowqGg3+TT9cQF/AJ6fLLRYjZII=" /> 
    
    <!-- Title Tag -->
	<title>Play Online Games | Free Online Games | Online Gaming | Play Free Games</title>
    
    <!-- Bootstrap -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/tb_overrides.css" rel="stylesheet">
    <link href="./css/tb_overrides_content.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <?php include_once('./google-analytics.php'); ?>

</head>

<body>

	<?php include_once('./navigation-main.php'); ?>
    
    <div class="container">
    	
        <div class="row inner">
        
        	<!-- Begin Right Column -->
        	<div class="col-md-9 inner">
                
                <div class="bg-primary hdr_push">
                    <h1>Free Online Games List</h1>
                    <h2>Suggestions on various browser based games</h2>
                </div>
                
                <!-- Begin Main Content -->
                <div class="col-lg-12">
                    
                    <p>Below we have compiled a list of free games from various spots around the web for something to do while bored. Some of these games are fairly modern while others
                    are quite old but oldies are goodies, right? Some are 100% free while others are <strong>free to play</strong> which means they are indeed free to access and play with most features but
                    they typically make it difficult to upgrade without paying. Oh, and you'll need the <a href="http://get.adobe.com/flashplayer/?promoid=BUIGP" target="_blank">Adobe Flash player</a> 
                    in order to view them properly.</p>
                    
                    <p><a href="http://www.tiberiumalliances.com/">Tibierum Alliances</a> [F2P]<br />
                    One of the best modern browser games to hit the market in 2012. It's slow and methodical but if you enjoy an RTS/Turn Based style with Command &amp; Conquer flavor mixed
                    in then this is the game for you.</p>
                    
                    <p><a href="http://www.cs2d.com/download.php" target="_blank" title="Play Counter Strike 2D">Play Counter Strike 2D</a><br />
                    A tried and true favorite reborn in 2D! It has all of the same weapons and game types such as hostage rescue (CS) and bomb defuse (DE). It can be a little awkward at 
                    times but once you get used to it, it's kinda fun.</p>
                    
                    <p><a href="http://worldoftanks.com/">World of Tanks</a> [F2P]<br />
                    If you're into tanks and blowing things up then this should be the perfect game for you. The World of Tanks free to play game is the first and only team-based massively multiplayer
                    online game dedicated to armored warfare. The game has over 150 vehicles to choose from, from such as America, Germany, France and the USSR. Users can also upgrade and 
                    customize historically accurate tanks across unique classes that fit player style, including light tanks, medium tanks, and heavy tanks. WOT is one of the fastest growing FTP games online.</p>
                    
                    <p><a href="http://worldoftanks.com/">Stronghold Kingdoms</a> [F2P]<br />
                    Yet another F2P game but in this game you are thrown back to the middle ages in an Age of Empires style setting. Users can play the role of a peaceful lord or battle it out to gain an
                    upper hand. You can besiege a castle that's never been taken, overthrow ruthless tyrants, bankroll your faction's war effort, pillage your neighbour's resources or even peacefully 
                    raise cattle!</p>
                    
                    <p><a href="https://www.carbongames.com/chrome.html">AirMech</a> [F2P]<br />
                    This is a futuristic action real-time strategy (ARTS) video game. Fun combo huh? It requires you to have quick FPS like skills yet have the mind of a chess master in regard to RTS play.
                    It has many modes ranging from solo, co-op, capture the flag to even Survival mode.</p>
                    
                    <p><a href="http://battlestar-galactica.bigpoint.com/">Battlestar Galactica Online</a> [F2P]<br />
                    It is what it says, the online gaming version of the cult hit tv show, Battlestar Galatica. You can take up daring missions, engage in space dogfights and explore the gritty world of BSG here.</p>
                    
                    <p><a href="http://www.swtor.com/">Star Wars The Old Republic</a> [F2P]<br />
                    If you're wanting to jump even further out into space then we suggest Star Wars The Old Republic. It is the only MMO game with a free to play option that puts you at 
                    the center of your own story-driven Star Wars saga. You can play as a Jedi, a Sith, a Bounty Hunter or any other Star Wars character and explore an 
                    age over three-thousand years before the classic films.</p>  
                    
                </div>
                <!-- End Main Content -->
            
            </div>
            <!-- End Left Column -->

			<!-- Begin Right Column -->
            <div class="col-md-3">
                
                <?php include_once('./navigation-side.php'); ?>
            
            </div>
            <!-- End Right Column -->
        
        </div>
    
    </div>
    <!-- End Main Container -->

	<?php include_once('./footer.php'); ?>
    
    <?php include_once('./global-js.php'); ?>
    
</body>

</html>