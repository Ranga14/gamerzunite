<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php
	include_once('./includes/header.includes.php');
	
	// Pull News Thread Data
	$Thread				= "SELECT tid,title,description,posts,start_date FROM ibf_topics WHERE forum_id='84' AND approved='1' ORDER BY start_date DESC LIMIT 20";
	$ThreadQuery		= mysqli_query($mysqli, $Thread);
		
	// Pull Lan (U.S.) Data
	$Lan				= "SELECT tid,title,posts,forum_id FROM ibf_topics 
						   WHERE forum_id='5' OR forum_id='6' OR forum_id='9' OR forum_id='11' OR forum_id='12' OR forum_id='13' OR forum_id='46'
						   OR forum_id='40' OR forum_id='41' OR forum_id='42' OR forum_id='43' OR forum_id='44' OR forum_id='45' OR forum_id='68'
						   OR forum_id='70' OR forum_id='69' OR forum_id='71' OR forum_id='72' OR forum_id='78' OR forum_id='80'
						   ORDER BY start_date DESC LIMIT 10";
	$LanQuery			= mysqli_query($mysqli, $Lan);
	
	// Pull Lan (UK) Data
	$LanUK				= "SELECT tid,title,posts,forum_id FROM ibf_topics WHERE forum_id='8' OR forum_id='37' ORDER BY start_date DESC LIMIT 8";
	$LanUKQuery			= mysqli_query($mysqli, $LanUK);
	
	// Pull Lan (UK) Data
	$LanCanada			= "SELECT tid,title,posts,forum_id FROM ibf_topics WHERE forum_id='7' ORDER BY start_date DESC LIMIT 8";
	$LanCanadaQuery		= mysqli_query($mysqli, $LanCanada);
	
	// Pull Latest Forum Post (General)
	$DisGen				= "SELECT tid,title,posts,forum_id FROM ibf_topics WHERE forum_id='14' ORDER BY start_date DESC LIMIT 8";
	$DisGenQuery		= mysqli_query($mysqli, $DisGen);
	
	// Pull Latest Forum Post (Console)
	$DisConsole			= "SELECT tid,title,posts,forum_id FROM ibf_topics WHERE forum_id='15' ORDER BY start_date DESC LIMIT 8";
	$DisConsoleQuery	= mysqli_query($mysqli, $DisConsole);
	
	// Pull Latest Forum Post (PC)
	$DisPC				= "SELECT tid,title,posts,forum_id FROM ibf_topics WHERE forum_id='16' ORDER BY start_date DESC LIMIT 8";
	$DisPCQuery			= mysqli_query($mysqli, $DisPC);
	
	// Pull Latest Forum Post (Online)
	$DisOnline			= "SELECT tid,title,posts,forum_id FROM ibf_topics WHERE forum_id='17' ORDER BY start_date DESC LIMIT 8";
	$DisOnlineQuery		= mysqli_query($mysqli, $DisOnline);
	
	// Pull Latest Forum Post (Handheld)
	$DisHand			= "SELECT tid,title,posts,forum_id FROM ibf_topics WHERE forum_id='25' ORDER BY start_date DESC LIMIT 8";
	$DisHandQuery		= mysqli_query($mysqli, $DisHand);
?>

<head>
	
	<!-- Title Tag -->
	<title>Online Gaming Community | Lan Parties | Lan Party Directory | Play Free Games</title>

	<!-- Meta Tags -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<meta name="keywords" content="Online Gaming Community, Online Gaming, Games, Gamers, Online Games, Gaming News, Lan Party, Lan Parties, 
    Lans, Lan, Gaming Events, Game Events, dallas lans, dallas lan parties, dfw lans, dfw lan parties, lan party games, play free games" />
	
	<meta name="description" content="Gamerz Unite - An Online Gaming Community for Gamers to find Lan Parties in their area." />
    
    <meta name="verify-v1" content="oIgbVOUJX3dUtEFWlowqGg3+TT9cQF/AJ6fLLRYjZII=" />
	
    <script src="/mint/?js" type="text/javascript"></script>
    <script type="text/javascript" src="http://yui.yahooapis.com/2.4.1/build/yahoo-dom-event/yahoo-dom-event.js"></script>
	<script type="text/javascript" src="http://yui.yahooapis.com/2.4.1/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="./scripts/script.js"></script>
	<script type="text/javascript" src="./js/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="./js/jquery.idtabs.min.js"></script>
    <script type="text/javascript" src="./js/jquery.scroll.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#news-container').vTicker({ 
				speed: 800,
				pause: 5000,
				animation: 'fade',
				mousePause: true,
				showItems: 1,
				height: 22
			});
		});
	</script>
 	
    <!-- Style Sheet & JS -->
    <style type="text/css" media="Screen">
    	/*\*/@import url("./css/master.css");/**/
	</style>
    <!--[if lt IE 7]>
		<link rel="stylesheet" href="css/ie6.css" type="text/css" media="screen" title="IE6" charset="utf-8" />
	<![endif]-->
	<!--[if gte IE 7]>
		<link rel="stylesheet" href="css/ie7.css" type="text/css" media="screen" title="IE7 or higher" charset="utf-8" />
	<![endif]-->
    
</head>

<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="PageContainer">

    <div id="Hdr_Top">
    
        <div id="FirstLine">
            <h1>Online Gaming Community</h1>&nbsp;for&nbsp;<h2>Gamers</h2>&nbsp;to find&nbsp;<h2>Lan Parties</h2>&nbsp;through our&nbsp;<h2>Lan Party Directory</h2> and <h2>Play Free Games</h2>.
        </div>

        <div id="TopRight"><a href="host-lan-party.php">Tips on Hosting a Lan Party &amp; Finding Lan Games</a></div>
    
    </div>
    
    <div id="Hdr_Adsense">
    
        <script type="text/javascript"><!--
        google_ad_client = "pub-7646919961525485";
        /* 728x15, created 2/8/09 */
        google_ad_slot = "6252231156";
        google_ad_width = 728;
        google_ad_height = 15;
        //-->
        </script>
        <script type="text/javascript"
        src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
        
    </div>    
    
    <div id="TopBanner">
        <div id="Hdr_Left">
            <a href="./index.php"><img src="graphics/hdr_left.jpg" height="115" width="580" alt="Online Gaming Community | Lan Parties | Lan Party Directory | Play Free Games" title="Online Gaming Community | Lan Parties | Lan Party Directory | Play Free Games" /></a>
        </div>
        <div id="Hdr_Right">
        	<a href="http://www.gamerzunite.com/forums/index.php?showforum=76" id="CnMore"></a>
        	<div class="hdr_padding">
                <table border="0" cellspacing="0" cellpadding="0" id="TableCn">
                <?php 
				while($cData	= mysqli_fetch_assoc($ContentQuery)) { $urlcdata = str_replace(' ', '-', $cData['title']);
					if ($cData['tid'] != 301) {
				?>
                    <tr><td><a href="./<?php echo $urlcdata; ?>"><? echo $cData['title']; ?></a></td><td style="text-align: right;"><? echo number_format($cData['posts']);  ?> Posts</td></tr>
				<? } } ?></table>
            </div>
        </div>
    </div>
                
    <div id="Middle">
    	
        <?php include_once('./navigation-main.php'); ?>
    
        <div id="News">
            <a href="./forums/index.php?showforum=18"><img src="graphics/hdr-latestgamingnews.jpg" height="64" width="580" alt="Games News | Gaming News" title="Games News | Gaming News" /></a>
            
            <div id="NewsText">
                <!-- News Begins -->
                <table border="0" cellspacing="0" cellpadding="0" id="TableNews">
                	<?php while($ThreadData	= mysqli_fetch_assoc($ThreadQuery)) { 
					//$time = mktime();
					//if ($ThreadData['topic_open_time'] > $time) {
						$urlNewsData = str_replace(" ", "-", $ThreadData['title']);
						//$final_url = str_replace("&#39;", "", $urlNewsData);
						?>
						<tr>
							<td><a href="./<?php echo $urlNewsData; ?>"><? echo $ThreadData['title']; ?></a><br /><div class="konafilter"><?php echo $ThreadData['description']; ?></div></td>
							<td><?php //$postDate = strftime("%b %d, %Y", $ThreadData['start_date']); echo "$postDate"; ?></td>
						</tr>
					<?php //} 
					} ?>
				</table>
                <!--<div class="MarginTop2"><a href="./forums/index.php?showforum=18" style="color: #999; font-size: 9px;">More</a></div> -->
                <!-- News Ends -->
                

                             	
                <div class="MarginTop2"></div>
            </div>
        </div>
        
        <div id="RightSide">
			<?php include_once('./navigation-side.php'); ?>
        </div>
        
        <div id="News_Bottom"></div>        	
    </div>
    
    <?php $img = array('bg_index2', 'bg_index3', 'bg_index4', 'bg_index5', 'bg_index6'); ?>
    <div style="background: url('./graphics/<?php echo $img[array_rand($img)]; //$ImageData['image_name'] ?>.jpg') no-repeat bottom; position: relative; border-bottom: 1px #999 dotted; width: 964px; margin: 0; clear: both;">
        <div id="BottomContentWrap">
                    
            <div class="float_left">
            	
                <div id="Navigation">
                    <a href="index.php">Home</a> . <a href="./forums/index.php?s=ca159dd1e9dc4745fb468db37545100b&amp;act=Reg&amp;CODE=00">Unite! (Join)</a> . <a href="./forums/">Forums</a> . <a href="./forums/index.php?showforum=5">Browse Lan / Events</a> . <a href="http://www.gamerzunite.com/forums/index.php?showtopic=15&amp;pid=65&amp;st=0&amp;#entry65">Free Hosting</a> . <a href="./play-free-games.php">Games</a>
                </div>
                
                <div id="MainBodyContent">
                                            
                    <h2>Welcome to Gamerz Unite <strong>Gaming Community</strong>!</h2>
        
                    <!--<p>Our site is dedicated to uniting gamers around the world and giving them a place to find lan parties, discuss their favorite games, gaming strategies, 
                    find and join clans or guilds, <a href="./play-free-games.php" style="text-decoration: none;">play free games</a>, read gaming news, apply 
                    for <a href="./forums/index.php?showtopic=15" style="text-decoration: none;">game web site hosting</a> and discuss relevant game topics as well as game 
                    issues that affect all gamers. </p>-->
                    
                    <p>Our site is dedicated to uniting gamers around the world and giving them a place to find  and post lan parties, discuss their favorite games and check out the latest
                    gaming articles or meme. <!--whether it be discussing 
                    the latest winner on the <a href="http://www.jackpotjoy.com/viewgames/deal-or-no-deal" target="_blank" style="text-decoration: none;">deal or no deal game</a> or how Mario Brothers is the 
                    best game ever-->. We also offer gaming strategies, find and join clans or guilds, <a href="./play-free-games.php" style="text-decoration: none;">play free games</a>, 
                    read gaming news, apply for <a href="./forums/index.php?showtopic=15" style="text-decoration: none;">game web site hosting</a> and discuss relevant game 
                    topics as well as game issues that affect all gamers.</p>
                    
                    <p>Lan parties are also a great way for gamers world wide to unite and discuss their experiences whether you play 
                    <a href="./forums/index.php?showforum=5" style="text-decoration: none;">lan games</a> 
                    <!--<a href="http://www.windowscasino.com/canada/free-slots/" target="_blank" style="text-decoration: none;">free slots at windows casino</a> games -->
                    or you just simply like gaming casually at home with friends.</p>

                    <p class="Margins">We provide Gaming News, Lan Party Directory, Gaming Clan Site Hosting for Free and Online Games to play. Click on 
                    the Lan Events link above to start your search of our directory of Lan Parties &amp; other Gaming Events. If your state or city is not 
                    listed in our menu above then tell us and we'll be sure to add it to our list.<!--, or for a better optimized search, 
                    discover <a href="http://onlinecasinoproject.com" target="_blank" style="text-decoration: none;">online casino reviews</a> while you play at the comfort of your home.--></p>
                    
                    <p>Learn more about <a href="./bingo_and_gaming.php" title="Bingo and Gaming">Bingo and Gaming</a>.</p>
                
                </div>
            
            </div>
    
            <div class="float_left margin_left">
        
                <div id="AffiliateStyle">
                    Affiliates&nbsp;&nbsp;<span class="GreyText">Click on any button below to go to an affiliate site.</span>
                </div>
                
                <!-- Affiliates Begin -->
                <ul id="Button">
                    <li>
                    	<a href="http://www.cncden.com/" title="CnC Den" target="_blank"><img src="http://www.cncden.com/cnc_new/cncden_button.gif" alt="CnC Den, Command &amp; Conquer Gaming Site." title="CnC Den, Command &amp; Conquer Gaming Site." height="31" width="88" /></a>
                        
                        <a href="http://www.texarkanagamers.com/" title="Texarkana Gamers" target="_blank"><img src="./graphics/tg_gamers.jpg" alt="Texarkanagamerz.com, Northeast Texas Gamers." title="Texarkanagamerz.com, Northeast Texas Gamers." height="31" width="88" /></a>
                        
                        <a href="http://www.networkgamingclub.com/" title="Network Gaming Club" target="_blank"><img src="http://www.gamerzunite.com/graphics/btn_ngc.jpg" height="31" width="88" border="0" alt="Network Gaming Club" title="Network Gaming Club"/></a>   
     
                        <a href="http://lifetimegamer.blogspot.com/" title="Lifetime Gamer" target="_blank"><img src="./graphics/btn_ltg.jpg" alt="Lifetime Gamer.com" title="Lifetime Gamer.com" height="31" width="88" /></a>
       
                        <a href="http://www.gamesmachine.net/" title="Games Machine" target="_blank"><img src="http://www.gamesmachine.net/includes/gamesmachine.png" height="31" width="88" border="0" alt="GamesMachine" title="GamesMachine"/></a>   
                    </li>
                    <li>
                        <a href="http://www.xlangaming.net/" title="X Lan Gaming" target="_blank"><img src="http://www.gamerzunite.com/graphics/btn_xlangaming.jpg" height="31" width="88" border="0" alt="XLAN Gaming" title="XLAN Gaming" /></a>
        
                        <a href="http://www.gamelemon.com/" title="Game Lemon" target="_blank"><img src="http://www.gamerzunite.com/graphics/btn_gllogo.jpg" height="31" width="88" border="0" alt="Game Lemon - Game News with a Funny Twist" title="Game Lemon - Game News with a Funny Twist" /></a>
                        
                        <a href="http://www.afterlifegaming.net/" title="Afterlife Gaming" target="_blank"><img src="http://www.gamerzunite.com/graphics/btn_afterlife.gif" height="31" width="88" border="0" alt="Afterlife - The Zombie Gaming Community" title="Afterlife - The Zombie Gaming Community" /></a>
                        
                        <a href="http://www.lanmaniac.com/" title="Lan Maniac" target="_blank"><img src="http://www.lanmaniac.com/Images/lmbutton.gif" alt="Southern California LAN Party - Bellflower, Los Angeles" title="Southern California LAN Party - Bellflower, Los Angeles" width="88" height="31" border="0" /></a>
                    
                    	<a href="http://www.catacombz.com/" title="Lan Maniac" target="_blank"><img src="http://www.catacombz.com/images/Combz-Button.gif" alt="Catacombz" title="Catacombz" width="88" height="31" border="0" /></a>
                    </li>
                    <li>
                    	<a href="http://www.gamesprays.com" title="Game Sprays : Team Fortress 2 Sprays and Logos" target="_blank"><img src="http://www.gamesprays.com/images/banners/gs_mini_banner.gif" alt="Game Sprays : Team Fortress 2 Sprays and Logos" title="Game Sprays : Team Fortress 2 Sprays and Logos" width="88" height="31" border="0" /></a>
                    
                    	<a href="http://www.lanpartyeh.com" target="_blank"><img src="http://www.lanpartyeh.com/images/banner1_88x31.gif" alt="LAN Party EH. Uniting the social gaming scene in Canada!" title="LAN Party EH. Uniting the social gaming scene in Canada!" width="88" height="31" border="0" /></a>
                    </li>
                </ul>
                <!-- Affiliates End -->

          </div>
    
        </div>
    </div>
    
    
    
    <div id="Bottom_Links">
        <ul>
        	<li><a href="http://www.atkcommunity.com/" target="_blank">ATK Community</a></li>
        	<li><a href="http://www.gametopsites.com/" target="_blank">Game Top Sites</a></li>
            <li><a href="http://www.gamessiteslist.com" target="_blank">Free Online Games</a></li>
            <li><a href="http://www.gamerzunite.com/forums/index.php?showtopic=270" target="_blank">Advertise Here</a></li>
            <li><a href="http://www.gamerzunite.com/forums/index.php?showtopic=270" target="_blank">Advertise Here</a></li>
        </ul>
        
        <div id="Copyright">
        	&copy; COPYRIGHT <b>Gamerz Unite</b> 2005-<?php echo date('Y'); ?> ALL RIGHTS RESERVED | <a href="./sitemap.html" title="Gamerz Unite Site Map">Site Map</a> |
            <a rel="nofollow" href="./privacy-policy.php">Privacy Policy</a> | <a rel="nofollow" href="./disclaimer.php">Disclaimer</a>
        </div>    
    </div>
    
</div>

<map name="SocialMap" id="SocialMap">
  <area shape="rect" coords="8,4,57,54" href="http://www.myspace.com/gamerzunitee" target="_blank" alt="Gamerzunite.com on Myspace" />
  <area shape="rect" coords="60,4,108,54" href="http://www.facebook.com/home.php#/pages/Gamerz-Unite/69824489430" target="_blank" alt="Gamerzunite.com on Facebook" />
  <area shape="rect" coords="109,4,159,55" href="http://www.twitter.com/lanparties" target="_blank" alt="Gamerzunite.com on Twitter" />
</map>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3458038-6");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>

</html>