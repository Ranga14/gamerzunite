<!DOCTYPE html>
<html lang="en">

<?php include_once('./includes/header.includes.php'); 

	// Pull News Thread Data
	$Thread				= "SELECT tid,title,title_seo,description,posts,start_date,topic_firstpost FROM ibf_topics WHERE forum_id='76' AND approved='1' ORDER BY start_date DESC LIMIT 25";
	$ThreadQuery		= mysqli_query($mysqli, $Thread);
?>

<head>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<meta name="keywords" content="Gamerz Unite, video game articles, gaming articles, latest games articles, articles gaming, gaming industry articles, video game stories, video game articles blog">
	
	<meta name="description" content="A Piece of Our Mind on the Video Game Industry + Game Preview & Reviews.">
    
    <meta name="verify-v1" content="oIgbVOUJX3dUtEFWlowqGg3+TT9cQF/AJ6fLLRYjZII="> 
    
    <!-- Title Tag -->
	<title>Game Articles | Game Previews | Game Reviews } Gaming OpEd</title>
    
    <!-- Bootstrap -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/tb_overrides.css" rel="stylesheet">
    <link href="./css/tb_overrides_content.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <?php include_once('./google-analytics.php'); ?>

</head>

<body>

	<?php include_once('./navigation-main.php'); ?>
    
    <div class="container">
    	
        <div class="row inner innerDark">
        
        	<!-- Begin Right Column -->
        	<div class="col-md-9 inner innerDark">
                
                <div class="bg-primary hdr_push">
                    <h1>Game Articles, Previews and Reviews</h1>
                    <h2>Just a little Piece of our Mind</h2>
                </div>
                
                <!-- Begin Main Content -->
                <div class="col-lg-12">
                    
                    <?php while($data = mysqli_fetch_assoc($ThreadQuery)):
					
						$url = $data['title'];
	
						// Pull Large Image for Article
						$ContentImg			= "SELECT attach_location FROM ibf_attachments WHERE attach_rel_id='$data[topic_firstpost]'";
						$ContentImgQuery	= mysqli_query($mysqli, $ContentImg);
						$ContentImg			= mysqli_fetch_array($ContentImgQuery);
						
						?>
				
						<a class="panel panel-default" style="background: url('./forums/uploads/<?php echo $ContentImg['attach_location']; ?>') top left no-repeat;" href="./<?php echo $data['title_seo']; ?>">
							<div class="panel-body">
								<h3 class="panel-title"><?php echo $data['title']; ?></h3>
							</div>
						</a>
					
					<?php endwhile; ?>
				
                </div>
                <!-- End Main Content -->
            
            </div>
            <!-- End Left Column -->

			<!-- Begin Right Column -->
            <div class="col-md-3">
                
                <?php include_once('./navigation-side.php'); ?>
            
            </div>
            <!-- End Right Column -->
        
        </div>
    
    </div>
    <!-- End Main Container -->

	<?php include_once('./footer.php'); ?>
    
    <?php include_once('./global-js.php'); ?>
    
</body>

</html>